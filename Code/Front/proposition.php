<?php
$title = "Proposition";
include('partials/head.php'); ?>

    <body style="background: linear-gradient(to right, #ffeeee, #ddefbb);">

<?php include('Partials/menuBarConnected.php'); ?>

    <div class="mainGreatBlockProposition">
        <div id="greatBlockProposition">
            <section class="greatLeftBlockProp">
                <section class="leftBlockProp">
                    <div id="leftBlockChildProp">
                        <img src="assets/images/img_users/luke.png">
                    </div>
                    <div id="first-name-profile">
                        <h4>Prénom</h4>
                    </div>
                    <div id="city-name-profile5">
                        <h4>
                            <i class="fas fa-map-marker-alt"></i>
                            Ville
                        </h4>
                    </div>
                    <div class="star-rating-profile">
                        <div>
                            <img src="assets/images/star-rating.png" width="30px;">
                        </div>
                        <div>
                            <article>4/5</article>
                        </div>
                    </div>
                    <div id="propositionQuestionBtn">
                        <button type="button" class="btn btn-success">Lui poser une question</button>
                    </div>
                    <div id="propositionBtn2">
                        <button type="button" class="btn btn-success">Lui faire une proposition</button>
                    </div>
                </section>
            </section>
            <section class="greatRightBlockProp">
                <section class="rightBlockProp">
                    <div id="priceDateProp">
                        <div class="dateProp">
                            <article>Il y a 1 heure</article>
                        </div>
                        <div class="priceProp1">
                            <article>30€ / heure</article>
                        </div>
                    </div>
                    <div class="mainTitleProp">
                        <div id="mainTitleProp2">
                            <h2>Cherche coiffure à domicile - Bayonne</h2>
                        </div>
                    </div>
                    <div class="mainDescripProp">
                        <div class="titleDescrip">
                            <article>Description :</article>
                        </div>
                        <div id="descripProp2">
                            <article>Bonjour, je recherche une coiffeuse à domicile pour le prochain week-end.</article>
                        </div>
                    </div>
                    <div class="cityMainProp">
                        <div class="titleCity90">
                            <article>Ville :</article>
                        </div>
                        <div class="CityUserProp">
                            <article>Bayonne</article>
                        </div>
                    </div>
                    <div class="cityMainProp">
                        <div class="titleCity90">
                            <article>Catégorie :</article>
                        </div>
                        <div class="CityUserProp">
                            <article>Coiffure à domicile</article>
                        </div>
                    </div>
                    <div id="localisationProp">
                        <article>Localisation :</article>
                    </div>
                </section>
            </section>
        </div>
        <section id="propositionMap">
            <div id="propositionMap2">
                <img src="assets/images/map-paris4.png">
            </div>
        </section>
    </div>

<?php 
        include('Partials/footer.php');
        include('Partials/scriptLinksBootstrap.php');
    ?>