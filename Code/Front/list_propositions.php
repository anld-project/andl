<?php
require_once('functions.php');
require_once('./assets/class/daba.class.php');

$title = "Liste des propositions";
include('Partials/head.php');

$bidNeed = $_SESSION['lpbidneed'];

if ($_GET)
{
    if(isset($_GET['bidneed']) AND !empty($_GET['bidneed']))
    {
        $bidNeed = $_GET['bidneed'];
        $_SESSION['lpbidneed'] = $bidNeed;
    }
}

?>

<script src="assets/JavaScript/list_proposition.js" async></script>
   
    <body>
    
<?php include('Partials/menuBarConnected.php'); ?>


    <div id="page-prop2">
        <div id="center-prop">
    <section id="standards-prop">
        <div id="title-standard1">
            <h4>Choisissez vos critères</h4>
        </div>
        
         
        <div id="btn-radio-prop">

            <div class="form-check-inline">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="optradio"<?php if ($bidNeed=="NEED") echo " Checked"; ?>>Les besoins
                </label>
            </div>

            <div class="form-check-inline">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="optradio"<?php if ($bidNeed=="BID") echo " Checked"; ?>>Les offres
                </label>
            </div>
        </div>
        
        
        

        <div id="city-prop">
            <div id="city-prop1">
                <div id="title-city1">
                    <h4>Près de chez vous</h4>
                </div>
            </div>
            <div id="title-city2">
                    <div id="fieldCity" class="input-group pt-4">
                        <input class="cityFormHome2" type="text" class="form-control" placeholder="Ville"
                            aria-label="" aria-describedby="basic-addon1">
                    </div>
                <div class="btn-group" id="perimeter1">
                    <button type="button" class="btn btn-default">Périmètre</button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">    <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#" title="5_km">1 km</a></li>
                        <li><a href="#" title="10_km">5 km</a></li>
                        <li><a href="#" title="20_km">10 km</a></li>
                        <li><a href="#" title="30_km">20 km</a></li>
                        <li><a href="#" title="50_km">50 km</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
            
    <!-- Début vignette proposition -->
            

        <?php
    
            $dab = new Daba(0,ORDER_DESC);
            $nb_ad = $dab->getNbad();
        ?>

        <?php for ($i = 0; $i < $nb_ad; $i++): ?>
        <?php $dab->NextAd(); ?>
        <?php   if (    ($bidNeed == '' || $bidNeed == 'ALL')
                        || ($bidNeed == 'NEED' && $dab->getAd()->needBidAttribute == 'NEED')
                        || ($bidNeed == 'BID' && $dab->getAd()->needBidAttribute == 'BID')
                   ):
        ?>
           
        <div class="add-prop">
            
            <?php if (isset($_SESSION['idUser'])): ?>
                <a href="deal_formulation.php?idAdLastProp=<?php echo $dab->getAd()->id; ?>">
            <?php elseif (!isset($_SESSION['idUser'])): ?>
                <a href="login.php">
            <?php endif; ?>
                    
            <div class="add-prop2">
                <div id="indent-prop">
                    <div id="photo-prop">
                        <img src="<?php echo $dab->getUsePortrait(); ?>">
                    </div>
                    <div id="first-name-prop"><strong><?php echo $dab->getUse()->firstName ?></strong></div>
               </div>
                <div id="part-tittle-add">
                    
                    <div id="main-title-add">
                         <h4><span>
                        <?php if ($dab->getAd()->needBidAttribute == 'NEED') echo "Je cherche";
                        else echo "Je loue"; echo " " . $dab->getObj()->label; ?>
                        </span><span> à </span><span><?php echo $dab->getUse()->addressCity; ?></span></h4>
                    </div>
                    
                    <div id="descrip-add-prop">
                        <article><?php echo $dab->getAd()->introductionText; ?></article>
                    </div>

                    <div id="city-and-date-prop">
                        <div id="city-prop2">
                            <div>
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                            <div>
                                <h6><?php echo $dab->getUse()->addressCity; ?></h6>
                            </div>
                        </div>
                        <div id="date-prop2">
                            <h6><?php echo "le ".$dab->getAd()->creationDate; ?></h6>
                        </div>
                    
                    </div>
                </div>
                <div id="part-price-add">
                    <div id="price-add3-<?php if ($dab->getAd()->needBidAttribute == 'NEED') echo "need"; else echo "bid"; ?>">
                        <article><?php echo $dab->getAd()->pricing; ?>
                        € <?php if ($dab->getAd()->leasingSaleAttribute == 'LEASING') echo " heure"; ?></article>
                    </div>
            </div>
            </div>
        </div>
            
        <?php endif; ?>
        <?php endfor; ?>     
           
    <!-- Fin vignette proposition -->
            
    </div>
 </div>

        
<?php
        include('Partials/footer.php');
        include('Partials/scriptLinksBootstrap.php');
    ?>