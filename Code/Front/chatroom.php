<?php
$title = "Messagerie";
require_once('functions.php');
require_once('./assets/class/chat.class.php');
include('./Partials/head.php');

define('CHATROOM_FILE_NAME', "chatroom");
?>

<head>

    <link rel="stylesheet" href="./assets/css/<?php echo CHATROOM_FILE_NAME; ?>.css">

</head>

<body>

<?php include('./Partials/menuBarConnected.php'); ?>

<?php

$idUserMe = $_SESSION['idUser'];
//echo "SESSION USER ID = (".$idUserMe.")<br>";
$idUserMate = 0;
$dealStatus = DEAL_NONE;
$adelete = 0;

if ($_GET) {
    if ($_GET['chatroom']) {
        $idChatroomActive = $_GET['chatroom'];
        $_SESSION['chatroom'] = $idChatroomActive;
    }
    if ($_GET['adelete']) {
        $adelete = $_GET['adelete'];
        setAdStatusRetired($adelete, 'RETIRED_PAUSE');
        $_SESSION['chatroom'] = 0;
    }
    if ($_GET['idUser']) {
        $idUserMe = $_GET['idUser'];
        //echo "GET USER ID = (".$idUserMe.")<br>";
    }
    if ($_GET['onglet']) {
        $onglet = $_GET['onglet'];
        $_SESSION['onglet'] = $onglet;
        //echo "BOX BUTTON = (".$onglet.")<br>";
    }
}

$onglet = $_SESSION['onglet'];
if ($onglet == ONGLET_NEED) $ongletFilter = 'NEED';
elseif ($onglet == ONGLET_BID) $ongletFilter = 'BID';
else $ongletFilter = '';

$idChatroomActive = $_SESSION['chatroom'];

?>

<main id="BoxMain<?php if (NB_PAGE_USER == 2) echo 'Duo'; ?>">

    <!-- de PAGE_USER_TOP a PAGE_USER_BOTTOM
   (en demo double page top et bottom. hors demo : page top seulement ) -->

    <?php for ($rui = 0; $rui < NB_PAGE_USER; $rui++): ?>

        <?php
        if ($rui == 0) $idUser = $idUserMe; else $idUser = $idUserMate;

        $chat_R = new Chat($idUser);

        $chat_L = new Chat($idUser);
        $cro = $chat_L->NextChat();

        //echo "CHAT SIDE (".$chat_L->getSide().")<br>";
        //echo "CRO AD RIGHT (".$cro->idAdRight.")<br>";
        //echo "CRO AD LEFT (".$cro->idAdLeft.")<br>";

        if ($chat_L->getSide() == CHAT_RIGHT) {
            $idAdRight = $cro->idAdRight;
            $idAdLeft = $cro->idAdLeft;
        } else {
            $idAdRight = $cro->idAdLeft;
            $idAdLeft = $cro->idAdRight;
        }

        //echo "ID AD RIGHT (".$idAdRight.")<br>";
        $chat_L->seekAd($idAdRight);
        $picUserRight = $chat_L->getUsePortrait();

        //echo "ID AD LEFT (".$idAdLeft.")<br>";
        $chat_L->seekAd($idAdLeft);
        $idUserMate = $chat_L->getAd()->idUser;  //id user page basse en mode demo

        unset($chat_Y);

        if ($_GET) {
            if ($rui == 0) {
                if ($_GET['tweetMe']) {
                    StoreTweet($idChatroomActive, $idUserMe, $_GET['tweetMe']);

                }
                if ($_GET['tweetMate']) {
                    StoreTweet($idChatroomActive, $idUserMate, $_GET['tweetMate']);

                }
            }
        }

        ?>

        <section class="RoomUser">

            <!---------------- BOX USER ME ---------------->

            <section id="BoxUserMe" >

                <div id="BoxOnglet">


                    <div id="BoxChoix">

                        <a id="BoxBbutton<?php if ($onglet == ONGLET_NEED) echo "Filled"; ?>"
                           href="<?php echo CHATROOM_FILE_NAME; ?>.php?onglet=<?php echo ONGLET_NEED; ?>">Besoins</a>
                        <a id="BoxObutton<?php if ($onglet == ONGLET_BID) echo "Filled"; ?>"
                           href="<?php echo CHATROOM_FILE_NAME; ?>.php?onglet=<?php echo ONGLET_BID; ?>">Offres</a>
                            <hr>
                    </div>


                </div>

                <div id="BoxAd">

                    <?php while (($cro = $chat_R->NextChat()) != false): ?>
                        <?php if (($ad = $chat_R->seekAdSideRight()) != false): ?>
                            <?php if ((($ongletFilter == '') || ($ongletFilter == $chat_R->getAd()->needBidAttribute))
                                && ($chat_R->getAd()->retiredStatus == 'RETIRED_OFF')
                            ): ?>

                                <div id="<?php if ($idChatroomActive == $cro->id) echo "BoxAdItemYellow"; else echo "BoxAdItemBlack"; ?>">

                                    <div id="ad_user">

                                        <div id="identMsgLeft">

                                            <div class="msgLeftCard1">
                                                <img id="ad_img" class="roundedImage"
                                                     src="<?php $uplAttribut = $chat_R->getUsePortrait();
                                                     echo $uplAttribut; ?>">
                                            </div>

                                            <div class="msgLeftCard2">
                                                <h6 id="ad_prenom"><?php echo $chat_R->getUse()->firstName ?></h6>
                                            </div>

                                            <span class="croix-text croix"
                                                  cro-id="<?php echo $chat_R->getAd()->id ?>"></span>

                                        </div>

                                        <?php
                                        $nbAttribut = $chat_R->getAd()->needBidAttribute;
                                        if ($nbAttribut == 'NEED') {
                                            $ad_title_color = "ad_title_need";
                                            $ad_title_text = "Je cherche";
                                        } else {
                                            $ad_title_color = "ad_title_bid";
                                            if ($chat_R->getAd()->leasingSaleAttribute == 'SALE') $ad_title_text = "Je vend"; else $ad_title_text = "Je loue";
                                        }
                                        if ($idChatroomActive == $cro->id) {
                                            $needBidAdActive = $nbAttribut;
                                            $userPicActive = $uplAttribut;
                                        }
                                        ?>

                                        <div id="<?php echo $ad_title_color; ?>" style="flex-grow: 1">
                                            <?php echo $ad_title_text;
                                            echo " " . $chat_R->getObj()->label; ?>

                                        </div>
                                        <div>
                                            <span class="croix-text croix" cro-id="<?php echo $chat_R->getAd()->id ?>"><i class="fas fa-times"></i></span>
                                        </div>
                                    </div>

                                    <div class="msgUseCardLeft" bad-id="<?php echo $cro->id ?>">

                                        <div id="ad_obj" style="flex-grow: 9">

                                            <div id="ad_text2"></div>
                                            <div id="ad_text2"><br><?php echo $chat_R->getAd()->introductionText; ?>
                                            </div>
                                            <div id="ad_text2"></div>

                                            <?php if ($chat_R->getShe()->startDate != '') : ?>
                                                <div id="ad_text2">
                                                    Entre <?php echo str_replace("00:00:00", "", $chat_R->getShe()->startDate); ?>
                                                    et <?php echo str_replace("00:00:00", "", $chat_R->getShe()->endDate); ?>
                                                    :
                                                </div>
                                            <?php else: ?>
                                                <?php echo "Depuis le " . $chat_R->getAd()->creationDate;
                                                ?>
                                            <?php endif; ?>

                                            <?php
                                            $nb_date = CountDate($chat_R->getShe());
                                            $seq = GetSheduleDate($chat_R->getShe());
                                            if ($nb_date != 0):
                                                ?>

                                                <?php for ($d = 0; $d < $nb_date; $d++): ?>
                                                <?php $date = NextDate($seq); ?>
                                                <div id="ad_text2">le <?php echo $date->dateHour;
                                                    echo ", " . $date->hourDuration . "heures"; ?> </div>
                                            <?php endfor; ?>

                                            <?php endif; ?>

                                            <div id="ad_text2"><br></div>
                                            <div id="ad_text2"><?php echo $chat_R->getAd()->pricing; ?>
                                                € <?php if ($chat_R->getAd()->leasingSaleAttribute == 'LEASING') echo " / heure"; ?></div>
                                        </div>

                                    </div>

                                </div>

                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endwhile; ?>

                </div>

            </section>

            <!------------------ BOX CHAT -------------------->

            <div id="BoxChat">

                <?php $chat_X = new Chat($idUser); ?>
                <?php while (($cro = $chat_X->NextChat()) != false): ?>

                    <?php if ($idChatroomActive == $cro->id): ?>

                        <?php

                        $dealStatus = $chat_X->getDeal();

                        if ($_GET) {
                            if (NB_PAGE_USER == 2) {
                                if ($_GET['deal_top'] && ($rui == 0)) {
                                    $dealStatus = $chat_X->nextDeal();
                                }
                                if ($_GET['deal_bottom'] && ($rui == 1)) {
                                    $dealStatus = $chat_X->nextDeal();
                                }
                            } else {
                                if ($_GET['deal']) {
                                    $dealStatus = $chat_X->nextDeal();
                                }
                            }
                        }

                        $ad = $chat_X->seekAdSideLeft();
                        ?>

                        <div id="BoxInfo">

                            <img id="BoxInfoImg" class="roundedImage" src="<?php echo $userPicActive; ?>">
                            <img id="BoxInfoImg" class="roundedImage"
                                 src="<?php echo $chat_X->getUsePortrait(); ?>">

                        </div>

                        <div id="BoxDuo" style="flex-grow: 9">

                            <?php $idPageMe = PAGE_USER_RIGHT;
                            $tableIdTweetSource =
                                [PAGE_USER_RIGHT, PAGE_USER_LEFT, PAGE_USER_RIGHT, PAGE_USER_LEFT];

                            $tweetList = GetTweet($cro->id);

                            for ($t = 0; $t < 4; $t++) {
                                if (isset($tweetList[$t])) {
                                    if ($tweetList[$t]->idUser == $idUser) $tableIdTweetSource[$t] = PAGE_USER_RIGHT;
                                    else $tableIdTweetSource[$t] = PAGE_USER_LEFT;
                                }
                            }

                            ?>

                            <!-- Affichage des 4 box duo -->

                            <?php for ($bhi = 4; $bhi > 0; $bhi--): ?>

                                <div id="BoxHistory">

                                    <?php
                                    $f = 1;
                                    // inversion orientation droit-gauche
                                    if ($idPageMe == $tableIdTweetSource[$bhi - 1]) $f = varinv($f);
                                    ?>

                                    <?php for ($i = 0; $i < 2; $i++): ?>

                                        <?php if (($f == $i) && isset($tweetList[$bhi - 1])): ?>
                                            <div id="BoxTweet">
                                                <div id="CadreTweet">
                                                    <?php echo $tweetList[$bhi - 1]->text; ?>
                                                </div>
                                                <div id="tweet_stamp"><?php echo $tweetList[$bhi - 1]->date; ?></div>
                                            </div>
                                        <?php else: ?>
                                            <div id="BoxTweetMask">
                                            </div>
                                        <?php endif; ?>

                                    <?php endfor; ?>

                                </div>

                            <?php endfor; ?>


                        </div>
                        <div id="BoxDeal" style="flex-grow: 1">
                            <?php
                            if (NB_PAGE_USER == 2) {
                                if ($rui == 0) $sens = '_top'; else $sens = '_bottom';
                            } else $sens = '';

                            if ($dealStatus == DEAL_NONE):
                                ?>
                                <a id="a_button"
                                   href="<?php echo CHATROOM_FILE_NAME; ?>.php?deal<?php echo $sens; ?>=x">DEAL</a><img
                                    id="BoxDealImg" src="<?php echo $chat_X->getPicDeal($dealStatus); ?>">
                                <h5><?php echo $chat_X->getTextDeal($dealStatus); ?></h5>
                            <?php elseif ($dealStatus == DEAL_WAIT): ?>
                                <a id="a_button"
                                   href="<?php echo CHATROOM_FILE_NAME; ?>.php?deal<?php echo $sens; ?>=x">DEAL</a><img
                                        id="BoxDealImg" src="<?php echo $chat_X->getPicDeal($dealStatus); ?>">
                                <h5><?php echo $chat_X->getTextDeal($dealStatus); ?></h5>
                            <?php else: //DEAL_DONE ?>
                                <a id="a_button"
                                   href="<?php echo CHATROOM_FILE_NAME; ?>.php?deal<?php echo $sens; ?>=x">DEAL</a><img
                                        id="BoxDealImg" src="<?php echo $chat_X->getPicDeal($dealStatus); ?>">
                                <h5><?php echo $chat_X->getTextDeal($dealStatus); ?></h5>
                            <?php endif; ?>
                        </div>
                        <div id="BoxInput" style="flex-grow: 1">
                            <?php if (($dealStatus == DEAL_DONE) && ($needBidAdActive == 'NEED')) : ?>
                                <a id="a_button" href="payment_notice.php?<?php echo "idAdMate=".$chat_X->getAd()->id; ?>" >PAYPAL<?php ?></a>
                            <?php elseif ($dealStatus != DEAL_DONE): ?>
                                <form id="tweet_input">
                                    <input type="text"
                                           name="<?php if ($rui == 0) echo "tweetMe"; else echo "tweetMate"; ?>"
                                           maxlength="100">
                                </form>
                            <?php endif; ?>
                        </div>

                        <?php $chat_Y = clone $chat_X; ?>

                    <?php endif; ?>
                <?php endwhile; ?>

            </div>

            <!--------------- BOX USER MATE ----------------->

            <div id="BoxUserMate">

                <?php if (isset($chat_Y)): ?>
                    <div class="imgMsgRight">
                        <img id="BoxMateImg" class="roundedImage"
                             src="<?php echo $chat_Y->getUsePortrait(); ?>">
                    </div>
                    <div id="BoxMatePrenom"><?php echo $chat_Y->getUse()->firstName ?></div>

                    <div id="BoxMateLocalisation">
                        <i class="fas fa-map-marker-alt"></i> <?php echo $chat_Y->getUse()->addressCity ?>
                    </div>
                    <div class="rating-chatroom">
                        <div>
                            <img src="assets/images/star-rating.png" width="22px";>
                        </div>
                        <div>
                            <article>4/5</article>
                        </div>
                    </div>
                <section id="mainBlockMsgBlack">
                    <div id="BoxAdItemBlack">
                        <section class="ad_main_user">
                            <div id="ad_user">
                                <div class="identCardRight">
                                    <div>
                                        <img id="ad_img" class="roundedImage"
                                             src="<?php echo $chat_Y->getUsePortrait(); ?>">
                                    </div>
                                    <h6 id="ad_prenom"><?php echo $chat_Y->getUse()->firstName ?></h6>
                                </div>
                            </div>
                            <div>

                                <?php
                                if ($chat_Y->getAd()->needBidAttribute == 'NEED') {
                                    $ad_title_color = "ad_title_need";
                                    $ad_title_text = "Je cherche";
                                } else {
                                    $ad_title_color = "ad_title_bid";
                                    $ad_title_text = "Je vend";
                                }
                                ?>

                                <div id="<?php echo $ad_title_color; ?>" style="flex-grow: 1">
                                    <?php echo $ad_title_text;
                                    echo " " . $chat_Y->getObj()->label; ?>
                                </div>
                                <?php if ($chat_Y->getAd()->retiredStatus != 'RETIRED_OFF'): ?>
                                    <div style="color:red;">***RETIRED***</div>
                                <?php endif; ?>

                            </div>
                        </section>
                        <div id="ad_obj" style="flex-grow: 9">

                            <div id="ad_text2"><br></div>
                            <div id="ad_text2"><?php echo $chat_Y->getAd()->introductionText; ?></div>

                            <?php if ($chat_Y->getAd()->retiredStatus == 'RETIRED_OFF'): ?>

                                <?php if ($chat_Y->getShe()->startDate != '') : ?>

                                    <div id="ad_text2">
                                        Entre <?php echo str_replace("00:00:00", "", $chat_Y->getShe()->startDate); ?>
                                        et <?php echo str_replace("00:00:00", "", $chat_Y->getShe()->endDate); ?> :
                                    </div>
                                <?php else: ?>
                                    <?php echo "Depuis le " . $chat_Y->getAd()->creationDate;
                                    ?>
                                <?php endif; ?>

                                <?php
                                $nb_date = CountDate($chat_Y->getShe());
                                $seq = GetSheduleDate($chat_Y->getShe());
                                if ($nb_date != 0):
                                    ?>

                                    <?php for ($d = 0; $d < $nb_date; $d++): ?>
                                    <?php $date = NextDate($seq); ?>
                                    <div id="ad_text2">le <?php echo $date->dateHour;
                                        echo ", " . $date->hourDuration . "heures"; ?> </div>
                                <?php endfor; ?>

                                <?php endif; ?>

                                <div id="ad_text2"><br></div>
                                <div id="ad_text2"><?php echo $chat_Y->getAd()->pricing; ?>
                                    € <?php if ($chat_Y->getAd()->leasingSaleAttribute == 'LEASING') echo "  / heure"; ?></div>

                            <?php endif; ?>

                        </div>


                    </div>
                </section>
                    <div id="BoxMateButton">
                        <a href="#">
                            <button type="button" class="btn btn-primary">Consulter le profil</button>
                        </a>

                    </div>

                <?php endif; ?>

            </div>


        </section>

    <?php endfor; ?>

</main>

<!-- Owl Carousel jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<!-- Owl Carousel JavaScript -->
<script type="text/javascript" src="/assets/JavaScript/owl.carousel.min.js" async></script>
<!-- Optional JavaScript -->
<script type="text/javascript" src="/assets/JavaScript/app.js" async></script>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<!-- The jQuery script of the slider -->
<script type="text/javascript" src="/assets/JavaScript/slider_moments.js" async></script>

<!-- chatroom specific -->
<script src="assets/JavaScript/chatroom.js"></script>

</body>


