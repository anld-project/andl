<?php
require_once('functions.php');
require_once('./assets/class/daba.class.php');

$title = "Espace utilisateur";
include('Partials/head.php');

$adretire = 0;
$toogleStatus = 'NONE';
$indexToogle = 0;

$bidNeed = $_SESSION['usbidneed'];
$idUser = $_SESSION['idUser'];


if ($_GET) {
    if (isset($_GET['bidneed']) AND !empty($_GET['bidneed'])) {
        $bidNeed = $_GET['bidneed'];
        $_SESSION['usbidneed'] = $bidNeed;
    }

    if (isset($_GET['adretire']) AND !empty($_GET['adretire'])) $adretire = $_GET['adretire'];

    if (isset($_GET['adclear']) AND !empty($_GET['adclear'])) {
        clearAdRetired();
    }

    if (isset($_GET['adtoogleon']) AND !empty($_GET['adtoogleon'])) {
        $toogleStatus = 'RETIRED_PAUSE';
        $indexToogle = $_GET['adtoogleon'];
    }
    if (isset($_GET['adtoogleoff']) AND !empty($_GET['adtoogleoff'])) {
        $toogleStatus = 'RETIRED_OFF';
        $indexToogle = $_GET['adtoogleoff'];
    }
}

?>

<script src="assets/JavaScript/userspace.js" async></script>

<!--p id="log">DEBUG<p/-->

<style>

    .bor {
        border: 4px solid red;
    }

</style>


<body style="background: linear-gradient(to right, #ffeeee, #ddefbb);">

<?php include('Partials/menuBarConnected.php'); ?>
<section class="mainGreatUserspace container">
    <div class="mainUserspaceTitle">
        <div id="titleUserspace1">
            <h4>Mes annonces</h4>
        </div>
        <div id="btnTopRightUserspace">
            <div id="div_radio" class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-secondary">
                    <input type="radio" name="options" id="userspace_option1"> Mes besoins
                </label>
                <label class="btn btn-secondary" >
                    <input type="radio" name="options" id="userspace_option2"> Mes offres
                </label>
                <label class="btn btn-secondary">
                    <input type="radio" name="options" id="userspace_option2"> Toutes les catégories
                </label>
            </div>
        </div>
    </div>
    <div class="mainUserspace2">

        <?php
        $dab = new Daba($idUser, ORDER_DESC);
        $nb_ad = $dab->getNbad();
        ?>

        <?php for ($i = 0, $a = 1; $i < $nb_ad; $i++): ?>

            <?php
            $dab->NextAd();


            if (($bidNeed == "") || ($bidNeed == "ALL")
                || ($bidNeed == 'NEED' && $dab->getAd()->needBidAttribute == 'NEED')
                || ($bidNeed == 'BID' && $dab->getAd()->needBidAttribute == 'BID')
            ):
                ?>

                <?php

                $availableStatus = $dab->getAd()->availableStatus;
                $retiredStatus = $dab->getAd()->retiredStatus;
                $bad = "retired";

                if ($indexToogle == $a && $retiredStatus != 'RETIRED_ON') {
                    $retiredStatus = $toogleStatus;
                    $dab->setStatusRetired($retiredStatus);
                }

                if ($adretire == $a) {
                    $bad = "retired";
                    $retiredStatus = 'RETIRED_ON';
                    $dab->setStatusRetired($retiredStatus);
                }

                if ($retiredStatus != 'RETIRED_ON') {
                    if ($availableStatus == 'AVAILABLE' || $availableStatus == 'CURRENT') {
                        if ($dab->getAd()->needBidAttribute == 'NEED') $bad = "need"; else $bad = "bid";
                    } elseif ($availableStatus == 'FINISHED') {
                        $bad = "done";
                    }
                }
                ?>
                <!--Début carte annonce-->

                <section id="userspaceCard" bidneed='<?php echo $dab->getAd()->needBidAttribute; ?>'>

                    <div class="userspaceChildTopCard">
                        <div>
                            <h4>
                                <?php
                                if ($dab->getMom()->title == 'OBJECT' || $dab->getMom()->title == 'SERVICE') echo $dab->getObj()->label;
                                else echo $dab->getMom()->title;
                                ?>
                            </h4>
                        </div>


                        <div>
                            <?php $m = false;
                            if ($dab->getMom()->title != 'OBJECT' && $dab->getMom()->title != 'SERVICE'): ?>

                                <li class="nav-item dropdown">

                                    <a class="nav-link dropdown-toggle" href="#" id="menuMomentUser" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-search-plus"></i>
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

                                        <a class="dropdown-item" href="#"><strong>-- Service --</strong></a>
                                        <?php
                                        $idob = GetObjectsAd($dab->getAd()->id);
                                        for ($j = 0; $j < count($idob); $j++):
                                            $obj = NextObject($idob, $j, ATR_NONE);
                                            if ($obj->attribut == ATR_SERVICE):
                                                ?>
                                                <a class="dropdown-item" href="#"><?php echo $obj->label; ?></a>
                                            <?php endif; ?>
                                        <?php endfor; ?>

                                        <a class="dropdown-item" href="#"><strong>-- Objet --</strong></a>
                                        <?php
                                        $idob = GetObjectsAd($dab->getAd()->id);
                                        for ($j = 0; $j < count($idob); $j++):
                                            $obj = NextObject($idob, $j, ATR_NONE);
                                            if ($obj->attribut == ATR_OBJECT):
                                                ?>
                                                <a class="dropdown-item" href="#"><?php echo $obj->label; ?></a>
                                            <?php endif; ?>
                                        <?php endfor; ?>

                                    </div>

                                </li>

                                <?php $m = true; endif; ?>

                        </div>


                        <div>
                        <span class="badge badge-success-<?php echo $bad; ?> <?php if ($retiredStatus == 'RETIRED_PAUSE') echo " opacity"; ?>"><?php echo $dab->getDisplayStatus($availableStatus, $retiredStatus);
                            ?></span>
                        </div>
                    </div>


                    <div class="recapUserspace">
                        <div class="recapChildUserspace">
                            <article class="recapAdLeft">Lieu</article>
                        </div>
                        <div class="recapChildUserspace">
                            <article class="recapAdRight"><?php echo $dab->getUse()->addressCity; ?></article>
                        </div>
                        <div class="recapChildUserspace">
                            <article class="recapAdLeft">Budget</article>
                        </div>
                        <div class="recapChildUserspace">
                            <article class="recapAdRight"><?php echo $dab->getAd()->pricing; ?>
                                € <?php if ($dab->getAd()->leasingSaleAttribute == 'LEASING') echo " heure"; ?></article>
                        </div>
                        <div class="recapChildUserspace">
                            <article class="recapAdLeft">Postée le</article>
                        </div>
                        <div class="recapChildUserspace">
                            <article class="recapAdRight"><?php echo " " . $dab->getAd()->creationDate; ?></article>
                        </div>

                        <div class="recapChildUserspace">
                            <article class="recapAdLeft">Publier</article>
                        </div>
                        <div class="recapChildUserspaceToogle">
                            <div class="material-switch pull-right ">
                                <input id="someSwitchOptionSuccess<?php echo $a; ?>"
                                       name="someSwitchOption<?php echo $a; ?>"
                                       type="checkbox"<?php if ($retiredStatus == 'RETIRED_PAUSE' || $retiredStatus == 'RETIRED_ON' || $availableStatus == 'NOT_AVAILABLE') echo " Checked"; ?>/>
                                <label for="someSwitchOptionSuccess<?php echo $a; ?>" class="label-success"></label>
                            </div>
                        </div>

                    </div>

                    <div id="btnCardUserspace<?php if ($m == true) echo "Moment"; ?>"
                         class="<?php if ($retiredStatus == 'RETIRED_ON' || $availableStatus == 'NOT_AVAILABLE') echo "opacity"; ?>">
                        <div id="retire">
                            <button type="button" class="btn btn-outline-danger">
                                <i class="fas fa-trash-alt"></i>
                                Retirer
                            </button>
                        </div>
                    </div>

                </section>

                <!--Fin carte annonce-->

                <?php $a++; endif; ?>
        <?php endfor; ?>

    </div>
    <div class="btnDropUser">
        <div>
            <button type="button" class="btn btn-outline-dark">
                Vider la corbeille
                <i class="fas fa-trash-alt"></i>
            </button>
        </div>
    </div>
</section>

<?php
include('Partials/footer.php');
include('Partials/scriptLinksBootstrap.php');
?>
