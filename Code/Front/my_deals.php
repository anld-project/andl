<?php
$title = "my DEALS";
require_once('functions.php');
require_once('./assets/class/user.class.php');
include('partials/head.php');

$_SESSION['page'] = "my_deals.php";

$idUser = $_SESSION['idUser'];
$lastDealAd = 0;

if ($_GET) {
    if ($_GET['lastdealad']) {
        $lastDealAd = $_GET['lastdealad'];
        updateLastUserDeal($idUser,$lastDealAd);
    }
}

$lastDealAd = NextUser(GetUser($idUser))->lastDealAd;

if ($lastDealAd){
    
$ad = GetAdId($lastDealAd);
$adobj = GetIdObject(GetAdMoment($ad)->id_object);
$use = NextUser(GetUser($ad->idUser));

if ($ad->priceAttribute != 'Global') $totalPrice = priceEstimate($ad->pricing,$ad->priceAttribute,$ad->duration);
else $totalPrice = $ad->pricing;
    
}

?>

<body style="background: linear-gradient(to right, #ffeeee, #ddefbb);">
<?php include('Partials/menuBarConnected.php'); ?>


<div class="mainBlockMyDeal">
    <div id="titleMyDeal">
        <div>
            <article>Mes DEALS</article>
        </div>
    </div>
    
    <?php if($lastDealAd): ?>
    
    <section class="MyDealMyDeal">

        <!-- Début vignette my DEAL -->

        <section id="thumbnailsMyDeal">
            <div class="infoContactDeal">
                <div id="photoMyDeal">
                    <div id="overflowDeal">
                        <img src="<?php echo GetUserPortrait($ad->idUser); ?>">
                    </div>
                    <div id="cityMyDeal">
                        <div id="cityMyDealChild">
                            <article>
                                <i class="fas fa-map-marker-alt"></i>
                                    <?php echo $use->addressCity; ?>
                            </article>
                        </div>
                    </div>
                </div>
                <div id="abstractInfoDeal">
                    <div>
                        <article><strong><?php echo $use->firstName; ?></strong></article>
                    </div>
                    <div>
                        <article><?php echo $use->mobileNumber; ?></article>
                    </div>
                    <div>
                        <article><?php echo $use->email; ?></article>
                    </div>
                </div>
            </div>
            <div class="btnMyDealView">
                <div>
                    <button type="button" class="btn btn-danger">Laisser un avis</button>
                </div>
            </div>
            <div class="summuaryMyDeal">
                <div class="summuaryMyDealChild">
                    <div>
                        <article><strong><?php echo $adobj->label; ?></strong></article>
                    </div>
                    <div>
                        <article>

                                <?php if($ad->priceAttribute != 'Global'): ?>
                                    <span>Prix total: </span><?php echo $totalPrice; ?></strong><strong> €</strong><br>
                                <?php else: ?>
                                    <span> Prix global: </span><strong><?php echo $totalPrice; ?></strong><strong> €</strong><br>
                                <?php endif; ?>
                        </strong>
                        </article>
                    </div>
                    <div>
                        <article>Paiement Paypal</article>
                    </div>

                    <!-- Les 2 prochaines lignes (durée de location et caution) sont nécessaires uniquement lors d'une location.
                          Lors d'un service seules les 3 premières sont suffisantes  -->

                    <div>
                        <article>Durée de location : <strong> <?php echo timeListConvert($ad->duration); ?></strong></article>
                    </div>
                    <div>
                        <article>Montant de la caution : <strong>30 €</strong></article>
                    </div>
                </div>
            </div>
            <div class="btnContactMyDeal">
                <div>
                    <button type="button" class="btn btn-danger">Contacter</button>
                </div>
            </div>
        </section>

        <!-- Fin vignette my DEAL -->

    </section>
    
    <?php endif; ?>
    
</div>


<?php 
    
    include('Partials/footer.php');
    include('Partials/scriptLinksBootstrap.php');
?>
