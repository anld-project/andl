<?php
$title ="Messagerie";
include('Partials/head.php'); ?>

<body style="background: linear-gradient(to right, #ffeeee, #ddefbb);">
<?php include('Partials/menuBarConnected.php'); ?>


<div class="container-fluid h-100 mt-3">
    <div class="row h-100" id="mainCardChatbox">
        <div class="col-md-4 col-xl-3 chat">
            <div class="card mb-sm-3 mb-md-0 contacts_card">
                <div class="card-header">
                    <div class="input-group">
                        <input type="text" placeholder="Search..." name="" class="form-control search">
                        <div class="input-group-prepend">
                            <span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
                        </div>
                    </div>
                </div>
                <div class="card-body contacts_body">
                    <ui class="contacts">
                        <li class="active">
                            <div class="d-flex bd-highlight">
                                <div class="img_cont">
                                    <img src="assets/images/img_users/tintin.png" class="rounded-circle user_img">
                                    <span class="online_icon"></span>
                                </div>
                                <div class="user_info">
                                    <span>Tintin</span>
                                    <p>Tintin est connecté</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex bd-highlight">
                                <div class="img_cont">
                                    <img src="assets/images/img_users/bart.png" class="rounded-circle user_img">
                                    <span class="online_icon offline"></span>
                                </div>
                                <div class="user_info">
                                    <span>Bart</span>
                                    <p>Bart est déconnectée depuis 7min</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex bd-highlight">
                                <div class="img_cont">
                                    <img src="assets/images/img_users/homer.png" class="rounded-circle user_img">
                                    <span class="online_icon"></span>
                                </div>
                                <div class="user_info">
                                    <span>Homer</span>
                                    <p>Homer est en ligne</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex bd-highlight">
                                <div class="img_cont">
                                    <img src="assets/images/img_users/lisa.png" class="rounded-circle user_img">
                                    <span class="online_icon offline"></span>
                                </div>
                                <div class="user_info">
                                    <span>Lisa</span>
                                    <p>Lisa est déconnectée depuis 7 min</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="d-flex bd-highlight">
                                <div class="img_cont">
                                    <img src="assets/images/img_users/asterix.png" class="rounded-circle user_img">
                                    <span class="online_icon offline"></span>
                                </div>
                                <div class="user_info">
                                    <span>Astérix</span>
                                    <p>Astérix est déconnecté depuis 7 min</p>
                                </div>
                            </div>
                        </li>
                    </ui>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-md-8 col-xl-6 chat">
            <div class="card">
                <div class="card-header msg_head">
                    <div class="d-flex bd-highlight">
                        <div class="img_cont">
                            <img src="assets/images/img_users/tintin.png" class="rounded-circle user_img">
                            <span class="online_icon"></span>
                        </div>
                        <div class="user_info">
                            <span>Discussion avec Tintin</span>
                            <p>1767 Messages</p>
                        </div>
                    </div>
                    <span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
                    <div class="action_menu">
                        <ul>
                            <li><i class="fas fa-user-circle"></i> Voir son profil</li>
                            <li><i class="fas fa-ban"></i> Signaler</li>
                        </ul>
                    </div>
                </div>
                <div class="card-body msg_card_body">
                    <div class="d-flex justify-content-start mb-4">
                        <div class="img_cont_msg">
                            <img src="assets/images/img_users/tintin.png" class="rounded-circle user_img_msg">
                        </div>
                        <div class="msg_cotainer">
                            Bonjour Luke ca va?
                            <span class="msg_time">8:40 AM, Aujourd'hui</span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end mb-4">
                        <div class="msg_cotainer_send">
                            Eh ben ! Comment il va?
                            <span class="msg_time_send">8:55 AM, Aujourd'hui</span>
                        </div>
                        <div class="img_cont_msg">
                            <img src="assets/images/img_users/luke.png" class="rounded-circle user_img_msg">
                        </div>
                    </div>
                    <div class="d-flex justify-content-start mb-4">
                        <div class="img_cont_msg">
                            <img src="assets/images/img_users/tintin.png" class="rounded-circle user_img_msg">
                        </div>
                        <div class="msg_cotainer">
                            Pas trop mal.
                            <span class="msg_time">9:00 AM, Aujourd'hui</span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end mb-4">
                        <div class="msg_cotainer_send">
                            Je pourrais te louer ta trottinette éléctrique?
                            <span class="msg_time_send">9:05 AM, Aujourd'hui</span>
                        </div>
                        <div class="img_cont_msg">
                            <img src="assets/images/img_users/luke.png" class="rounded-circle user_img_msg">
                        </div>
                    </div>
                    <div class="d-flex justify-content-start mb-4">
                        <div class="img_cont_msg">
                            <img src="assets/images/img_users/tintin.png" class="rounded-circle user_img_msg">
                        </div>
                        <div class="msg_cotainer">
                            Bien sûr. Pour quelle date?
                            <span class="msg_time">9:07 AM, Aujourd'hui</span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end mb-4">
                        <div class="msg_cotainer_send">
                            Le 2 janvier
                            <span class="msg_time_send">9:10 AM, Aujoutd'hui</span>
                        </div>
                        <div class="img_cont_msg">
                            <img src="assets/images/img_users/luke.png" class="rounded-circle user_img_msg">
                        </div>
                    </div>
                    <div class="d-flex justify-content-start mb-4">
                        <div class="img_cont_msg">
                            <img src="assets/images/img_users/tintin.png" class="rounded-circle user_img_msg">
                        </div>
                        <div class="msg_cotainer">
                            Ok. Rendez vous à mon domicile.
                            <span class="msg_time">9:12 AM, Aujourd'hui</span>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="input-group">
                        <div class="input-group-append">
                            <span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
                        </div>
                        <textarea name="" class="form-control type_msg" placeholder="Type your message..."></textarea>
                        <div class="input-group-append">
                            <span class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-sm-3 mb-md-0 contacts_card">
            <div class="card-body">
                <div id="leftBlockChildProp">
                    <img src="assets/images/img_users/tintin.png">
                </div>
                <div id="first-name-profile">
                    <h4>Prénom</h4>
                </div>
                <div id="city-name-profile5">
                    <h4>
                        <i class="fas fa-map-marker-alt"></i>
                        Ville
                    </h4>
                </div>
                <div class="star-rating-profile">
                    <div>
                        <img src="assets/images/star-rating.png" width="30px;">
                    </div>
                    <div>
                        <article>4/5</article>
                    </div>
                </div>
                <div id="propositionQuestionBtn">
                    <button type="button" class="btn btn-success">Lui poser une question</button>
                </div>
                <div id="propositionBtn2">
                    <button type="button" class="btn btn-success">Lui faire une proposition</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CDN link jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>

<!-- Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<!-- The jQuery script of the chatBox -->
<script type="text/javascript" src="assets/JavaScript/chatBox.js" async></script>

</body>
