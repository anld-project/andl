<?php

require_once('functions.php');
require_once('./assets/class/user.class.php');
include('partials/head.php');

$_SESSION['page'] = "deal_formulation.php";

if ($_GET) {
    if ($_GET['idAdLastProp']) {
        $idAdLastProp = $_GET['idAdLastProp'];
        $_SESSION['idAdLastProp'] = $idAdLastProp;
    }
}

//extraction des infos annonce slider page acceuil

$idAdLastProp = $_SESSION['idAdLastProp'];
$ad = GetAdId($idAdLastProp);
$adobj = GetIdObject(GetAdMoment($ad)->id_object);

//extraction des infos user courant

$idUser = $_SESSION['idUser'];
$user = new user($idUser);
$obj = $user->NextUser();

?>

<head>

    <!--script src = './assets/JavaScript/recap_deal_popup.js' async></script-->

</head>
<body>

<?php include('partials/menuBarConnected.php'); ?>

<div class="mainBlockCreatProposition">
    <section class="titlePropCreation">

        <?php if ($ad->needBidAttribute == 'NEED'): ?>
            <h4 style="color:rgba(9, 204, 35, 160);">Je propose... <?php echo $adobj->label; ?></h4>
        <?php else: ?>
            <h4 style="color:rgb(204, 35, 160);">je recherche... <?php echo $adobj->label; ?></h4>
        <?php endif; ?>

    </section>
    <section class="mainBlockPropCreat">
        <div class="mainChildBlockPropCreat">

            <div id="profilDealProp">
                <div id="photoDealProp">
                    <img src="<?php echo $user->getUsePortrait(); ?>">
                </div>
                <div id="infoProfilDealProp">
                    <div id="first-name-profile">
                        <h4><?php echo $obj->firstName; ?></h4>
                    </div>
                    <div id="city-name-profile5">
                        <h4>
                            <i class="fas fa-map-marker-alt"></i>
                            <?php echo $obj->addressCity; ?>
                        </h4>
                    </div>
                    <div class="star-rating-profile">
                        <div>
                            <img src="assets/images/star-rating.png" width="30px;">
                        </div>
                        <div>
                            <article>4/5</article>
                        </div>
                    </div>
                </div>
            </div>

            <div id="descripPropCreat">
                <div id="photoPropCreat">
                    <div class="top-modify-photo">
                        <div id="titleChoicePropCreat">
                            <article>Vos photos (si nécessaire)</article>
                        </div>
                    </div>
                    
                    
                    
                   
                    <div class="top-modify-photo">
                        <div class="container">
                            <div class="row">
                                <div class="imgUp">
                                    <div class="imagePreview">
                                        <label>
                                            <input type="file" class="uploadFile img" value="Upload Photo">
                                            <section class="iconUpload">
                                                <div class="iconUploadChild">
                                                <i class="far fa-images fa-3x"></i>
                                                </div>
                                            </section>
                                        </label>
                                    </div>
                                </div><!-- col-2 -->
                            </div><!-- row -->
                            <div class="imgAdd">
                                <i class="fas fa-plus"></i>
                            </div>
                        </div><!-- container -->
                    </div>
                    
                    
                    
                    

                </div>
            </div>

            <form method="get" action="recap_deal.php">

                <div id="descripPropCreat">
                    <div id="titleChoicePropCreat">
                        <article>Décrivez votre proposition de DEAL</article>
                    </div>
                    <div class="top-modify-photo">
                        <div class="descripCreatProposition6">
                            <form>
                                <div class="form-group">
                                    <textarea name="deal_text" class="form-control" id="descripCreatProp76" rows="3"
                                              placeholder="Bonjour,"></textarea>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="photoPropCreat">
                        <div id="conditionsPropDeal">
                            <div id="titleChoicePropDeal1">
                                <div class="tiltesConditionsDeal">
                                    <article>Vos conditions</article>
                                </div>
                                <div id="childConditionsPropCreat">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">

                                        <label class="btn btn-secondary">
                                            <input type="radio" name="option_heure" id="option1" checked> Heure
                                        </label>
                                        <label class="btn btn-secondary">
                                            <input type="radio" name="option_jour" id="option2"> Jour
                                        </label>
                                        <label class="btn btn-secondary">
                                            <input type="radio" name="option_global" id="option3"> Global
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div id="titleChoicePropDeal2">
                                <div class="tiltesConditionsDeal">
                                    <article>Budget</article>
                                </div>
                                <div class="choiceConditionsDeal">
                                    <select class="custom-select mr-sm-2" id="conditionsPriceDeal" name="option_budget">
                                        <option value="0" selected>5 €</option>
                                        <option value="1">10 €</option>
                                        <option value="2">15 €</option>
                                        <option value="3">20 €</option>
                                        <option value="4">25 €</option>
                                        <option value="5">30 €</option>
                                        <option value="6">35 €</option>
                                        <option value="7">40 €</option>
                                        <option value="8">45 €</option>
                                        <option value="9">50 €</option>
                                        <option value="10">60 €</option>
                                        <option value="11">70 €</option>
                                        <option value="12">80 €</option>
                                        <option value="13">90 €</option>
                                        <option value="14">100 €</option>
                                        <option value="15">120 €</option>
                                        <option value="16">150 €</option>
                                        <option value="17">200 €</option>
                                    </select>
                                </div>
                            </div>
                            <div id="titleChoicePropDeal2">
                                <div class="tiltesConditionsDeal">
                                    <article>Temps estimé</article>
                                </div>
                                <div class="choiceConditionsDeal">
                                    <select class="custom-select mr-sm-2" id="conditionsTimeDeal"
                                            name="option_duration">
                                        <option value="0" selected>inconnu</option>
                                        <option value="1">1 heure</option>
                                        <option value="2">2 heures</option>
                                        <option value="3">3 heures</option>
                                        <option value="4">1 demi-journée</option>
                                        <option value="5">1 journée</option>
                                        <option value="6">2 jours</option>
                                        <option value="7">3 jours</option>
                                        <option value="8">4 jours</option>
                                        <option value="9">5 jours</option>
                                        <option value="10">6 jours</option>
                                        <option value="11">1 semaine</option>
                                        <option value="12">2 semaines</option>
                                        <option value="13">3 semaines</option>
                                        <option value="14">1 mois</option>
                                        <option value="15">2 mois</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <section class="mainPostCreatProp">
                            <div id="btnPostCreatProp">
                                <?php if ($ad->needBidAttribute == 'NEED'): ?>
                                    <input id='bnext' type="submit" class="btn btn-bid">
                                <?php else: ?>
                                    <input id='bnext' type="submit" class="btn btn-need">
                                <?php endif; ?>
                            </div>
                        </section>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

<?php
include('partials/footer.php');
include('partials/scriptLinksBootstrap.php');
?>

</body>

