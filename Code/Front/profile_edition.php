<?php

require_once('functions.php');
require_once('./assets/class/user.class.php');

$_SESSION['page'] = "profile_edition.php";

//extraction des infos user courant

$idUser = $_SESSION['idUser'];
$user = new user($idUser);
$obj = $user->NextUser();

?>

<?php
$title = "Editer son profil";
include('Partials/head.php');
?>

<script src="assets/JavaScript/profile_edition.js" async></script>

<!--p id="log">DEBUG</p-->

<body style="background: linear-gradient(to right, #ffeeee, #ddefbb);">

<?php include('Partials/menuBarConnected.php'); ?>

<div class="main-profile-edition container-fluid">

    <div id="title-edition">
        <h3>Modifier mon profil</h3>
    </div>
    <!--form method="get" action="/profile.php"-->
        <div id="main-child-edition">
            <div class="modify-photo">
                <div id="title-modify-photo">
                    <article>Ma photo de profil</article>
                </div>
                <div id="underBlockPhoto">
                    <div id="photo-location">
                        <div class="imgPhotoProfile">
                            <div class="imagePreviewProfile">
                                <label>
                                    <input type="file" class="uploadFile img" value="Upload Photo">

                                    <section class="iconUpload">
                                        <div class="iconUploadChild">
                                            <i class="far fa-images fa-2x"></i>
                                        </div>
                                    </section>
                                </label>
                            </div>
                        </div><!-- col-2 -->
                        <div id="subTitlePhoto">
                            <div>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="post-presentation-edition">
                    <div>
                        <input type="submit" id='picinp' class="btn btn-success" value="Mettre à jour">
                    </div>
                </div>
            </div>

            <section id="EditCentralProfile">
            <form method="get" action="/profile.php">
                <div class="modify-mail-num">
                    <div class="modify-mail">
                        <div id="title-modify-photo">
                            <article>Mon adresse email</article>
                        </div>
                        <div id="modify-mail">
                            <div class="form-group">
                                <label for="exampleFormControlInput1"></label>
                                <input type="email" name="email" class="form-control" id="exampleFormControlInput1"
                                       placeholder="name@example.com" disabled/>
                            </div>
                        </div>
                    </div>
                    <div class="modify-num">
                        <div id="title-modify-photo">
                            <article>Mon numéro de téléphone</article>
                        </div>
                        <div id="modify-number">
                            <div class="form-group">
                                <input type="text" name="mobileNumber" class="form-control"
                                       id="exampleFormControlInput1" value="<?php echo $obj->mobileNumber; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="main-ident">
                    <section id="kindAndDate">
                        <div id="kindModify">
                            <div id="child-kindModify">
                                <article>Je suis</article>
                            </div>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-info">
                                    <input type="radio" name="option_male" id="option1" checked> Homme
                                </label>
                                <label class="btn btn-info">
                                    <input type="radio" name="option_female" id="option2"> Femme
                                </label>
                            </div>
                        </div>
                        <div id="birthdayModify">
                            <div class="child-birthdayModify">
                                <article>Date de naissance</article>
                            </div>
                            <div class="form-group row">

                                <div class="col-10">
                                    <input type="text" data-datedropper data-dd-format="d-m-Y" data-dd-lang="fr" data-dd-large="true" data-dd-large-default="false" data-dd-translate-mode="true" data-dd-event-selector="click" data-dd-theme="datepicker2-vopus" name="birthdayDate" value="<?php echo $obj->birthdayDate; ?>" id="example-date-input">
                                    <script> $('#example-date-input').dateDropper(); </script>

                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="ident-blockModify">
                        <div id="title-indent4">
                            <article>Indentité</article>
                        </div>
                        <div id="identity8">
                            <div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="firstName" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" placeholder="Prénom">
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="lastName" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" placeholder="Nom (facultatif)">
                                    <small id="emailHelp" class="form-text text-muted">Nous ne partageons jamais vos
                                        données</small>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div id="title-indent3">
                            <article>Mon adresse postale</article>
                        </div>
                        <div id="identity7">
                            <div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="addressCity"
                                           id="exampleInputEmail11" aria-describedby="emailHelp"
                                           placeholder="Ville">
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="addressPostalCode" id="inputCP11"
                                           aria-describedby="emailHelp" placeholder="Code postal">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="addressStreet" id="inputAddress"
                                   placeholder="rue">
                        </div>
                    </section>
                </div>
                <section id="main-description">
                    <div class="child-description">
                        <div id="title-indent3">
                            <article>Ma description</article>
                        </div>
                        <div class="little-child-description">
                            <form>
                                <div class="form-group">
                                        <textarea class="form-control" name="presentation"
                                                  id="exampleFormControlTextarea1" rows="4"
                                                  placeholder="Bonjour,"></textarea>

                                </div>
                            </form>
                        </div>

                        <div id="post-presentation-edition">
                            <div>
                                <input type="submit" class="btn btn-success" value="Mettre à jour">
                            </div>
                        </div>

                    </div>
                </section>
             </form>
           </section>

            <section id="main-modify-photos">
                <div class="child-modify-photo">
                    <div id="title-indent3">
                        <article>Mes photos</article>
                    </div>
                    <div class="top-modify-photo">

                        <!--First photo-->

                        <div class="container">
                            <div class="row">
                                <div class="imgUp">
                                    <div class="imagePreview">
                                        <label>
                                            <input type="file" class="uploadFile img" value="Upload Photo">

                                            <section class="iconUpload">
                                                <div class="iconUploadChild">
                                                <i class="far fa-images fa-3x"></i>
                                                </div>
                                            </section>
                                        </label>
                                    </div>
                                </div><!-- col-2 -->
                            </div><!-- row -->
                            <div class="imgAdd">
                                <i class="fas fa-plus"></i>
                            </div>
                        </div><!-- container -->

                    </div>
                </div>
            </section>
            
            <section id="modify-MP1">
                <div id="main-modify-MP1">
                    <div id="title-indent3">
                        <article>Modifier mon mot de passe</article>
                    </div>
                    <div id="modify-field-MP1">

                        <form method="post" action="/profile.php">

                            <div class="form-group">


                                <?php if ($_GET && $_GET['fail'] == 'true'): ?>
                                    <div class="alert alert-danger" role="alert">
                                        Vos mots de passe ne sont pas identiques !
                                    </div>
                                <?php endif; ?>

                                <input type="password" name="password_a" class="form-control"
                                       placeholder="Mot de passe">
                            </div>

                            <div class="form-group">
                                <input type="password" name="password_b" class="form-control"
                                       placeholder="Confirmer mot de passe">
                            </div>

                            <div id="post-modify-password2">
                                <div>
                                    <input type="submit" class="btn btn-success" value="Mettre à jour">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <section class="main-valid-modify">
                <div class="main-valid-modify2">
                    <div id="account-deleted">
                        <a href="profile_delete.php">
                            <button type="button" class="btn btn-outline-danger">Supprimer mon compte</button>
                        </a>
                    </div>
                </div>
            </section>
        </div>
</div>



<?php
include('Partials/footer.php');
include('Partials/scriptLinksBootstrap.php');
?>
