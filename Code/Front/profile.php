<?php

    require_once('functions.php');
    require_once('./assets/class/user.class.php');


    $idUser = $_SESSION['idUser'];

    $kmVisibility = 5;
    $verified = 0;
    $banned = 0;
    $dealCounter = 0;
    $userPictureLink = "user.png";
    $addressNumber = '';

    if ($_GET || $_POST)
    {
        
     //extraction des infos user courant avant modification
    $util = new user($idUser);
    $abj = $util->NextUser();

    $email = $abj->email;
    $mobileNumber = $abj->mobileNumber;
    $birthdayDate = $abj->birthdayDate;
    $gender = $abj->gender;
    $firstName = $abj->firstName;
    $lastName = $abj->lastName;
    $addressCity = $abj->addressCity;
    $addressPostalCode = $abj->addressPostalCode;
    $addressStreet = $abj->addressStreet;
    $presentation = $abj->presentation;
    $password = $abj->password;
    $password_a = ''; $password_b = ''; 

    if ($_GET)
    {
 
        if(isset($_GET['email']) AND !empty($_GET['email'])) $email = $_GET['email'];
        if(isset($_GET['mobileNumber']) AND !empty($_GET['mobileNumber'])) $mobileNumber = $_GET['mobileNumber'];
        if(isset($_GET['birthdayDate']) AND !empty($_GET['birthdayDate'])) $birthdayDate = $_GET['birthdayDate'];
        if(isset($_GET['option_female']) AND !empty($_GET['option_female'])) $gender = 'FEMALE';
        if(isset($_GET['firstName']) AND !empty($_GET['firstName'])) $firstName = $_GET['firstName'];
        if(isset($_GET['lastName']) AND !empty($_GET['lastName'])) $lastName = $_GET['lastName'];
        if(isset($_GET['addressCity']) AND !empty($_GET['addressCity'])) $addressCity = $_GET['addressCity'];
        if(isset($_GET['addressPostalCode']) AND !empty($_GET['addressPostalCode'])) $addressPostalCode = $_GET['addressPostalCode'];
        if(isset($_GET['addressStreet']) AND !empty($_GET['addressStreet'])) $addressStreet = $_GET['addressStreet'];
        if(isset($_GET['presentation']) AND !empty($_GET['presentation'])) $presentation = $_GET['presentation'];
         
    }

    if ($_POST)
    {
  
        if(isset($_POST['password_a']) AND !empty($_POST['password_a'])) $password_a = $_POST['password_a'];
        if(isset($_POST['password_b']) AND !empty($_POST['password_b'])) $password_b = $_POST['password_b'];
        
        if ($password_a == $password_b) $password = password_hash($_POST['password_a'], PASSWORD_DEFAULT);
        else { redirect("/profile_edition.php?fail=true"); }
    }
        
    //enregistrement des informations user courant
        
     storeUser(
                $idUser,
                $email,
                $password,
                $firstName,
                $mobileNumber,
                $lastName,
                $addressNumber,
                $addressStreet,
                $addressPostalCode,
                $addressCity,
                $kmVisibility,
                $verified,
                $banned,
                $dealCounter,
                $userPictureLink,
                $gender,
                $presentation,
                $birthdayDate
            );

    }; 


    //extraction des infos user apres modification - pour affichage ci-dessous

    $user = new user($idUser);
    $obj = $user->NextUser();

?>

<?php
    $title = "Profil";
    include('Partials/head.php');
?>

<body style="background: linear-gradient(to right, #ffeeee, #ddefbb);">
    
<?php include('Partials/menuBarConnected.php'); ?>

<div class="main-block-profile container">
        
    <section id="profile-mapping">
        <img src="<?php echo GetUserMap($obj->firstName); ?>">
    </section>
  
<div class="block-photo-btn">
    <!--Début section profil avec photo-->
    
    <section id="photo-profile">
        <div id="photo-child-profile">
            <img src="<?php echo $user->getUsePortrait(); ?>">
        </div>
        <div id="first-name-profile">
            <h4><?php echo $obj->firstName; ?></h4>
        </div>
        <div id="city-name-profile5">
            <h4><i class="fas fa-map-marker-alt"></i>
            <?php echo $obj->addressCity; ?></h4>
        </div>
        <div class="star-rating-profile">
            <div>
                <img src="assets/images/star-rating.png" width="30px;">
            </div>
            <div>
                <article>4/5</article>
            </div>
        </div>
    </section>

    <!--Fin section profile avec photo-->
    
    <form id="btn-modify-profile" action="profile_edition.php">
        <input type="submit" class="btn btn-success" value="Modifier mon profil">
    </form>
    
    
</div>
       
      <div id="main-presentation-profile">
            <div class="subTitleProfile">
                <h3>Présentation</h3>
            </div>
            <div class="block-profile26">
                <section id="block-presentation-profile7">
                    <article><?php echo $obj->presentation; ?></article>
                </section>
          </div>
    </div>
    <div id="photo-gallery-profile">
            <div class="subTitleProfile">
                <h3>Galerie photos des objets et services de <?php echo $obj->firstName; ?></h3>  
            </div> 
            <div class="block-profile26">
                <div id="picture-parent-profile">
                    
                    <!--First photo -->
                    
                    <div id="picture-child-profile">
                        <div id="little-modify-photo">
                            <div id="little-modify-photo2">
                                <img src="<?php echo $user->getUsePic().'/pic.png'; ?>">
                            </div>
                        </div> 
                    </div>
                    
                    <!--Second photo -->
                    
                     <div id="picture-child-profile">
                        <div id="little-modify-photo">
                            <div id="little-modify-photo2">
                                <img src="<?php echo $user->getUsePic().'/pic.png'; ?>">
                            </div>
                        </div>
                    </div>
                    
                    <!--Third photo -->
                    
                     <div id="picture-child-profile">
                        <div id="little-modify-photo">
                            <div id="little-modify-photo2">
                                <img src="<?php echo $user->getUsePic().'/pic.png'; ?>">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div id="profile-ratings">
            <div class="subTitleProfile">
                <h3>Evaluations de <?php echo $obj->firstName; ?></h3>
          </div> 
          <div class="block-profile26">
          </div>
    </div> 
</div>


<?php 
        include('Partials/footer.php');
        include('Partials/scriptLinksBootstrap.php');
    ?>
