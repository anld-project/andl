<div class="footer">
    <div>
        <div class="footer_child container">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-flex flex-row">
                <div class="ft-logo">
                    <img src="assets/images/logos/logo_vopus_simple.gif" alt="logo Vopus des économies à partager">
                </div>
                <div class="social_media_rejoin">
                    <div class="title-footer-social-link">
                        <div>
                            <article>Retrouvez nous sur les réseaux sociaux</article>
                        </div>
                    </div>
                    <div class="footer-social-link">
                        <div>
                            <a href="#">
                                <img src="assets/images/social_media/instagram.png" width="40px">
                            </a>
                        </div>
                    </div>
                    <div class="footer-social-link">
                        <div>
                            <a href="#">
                                <img src="assets/images/social_media/facebook.png" width="40px">
                            </a>
                        </div>
                    </div>
                    <div class="footer-social-link">
                        <div>
                            <a href="#">
                                <img src="assets/images/social_media/youtube.png" width="40px">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="footer-line">
        <section class="d-flex justify-content-center">
            <div id="main_contain_footer">
                <div id="a_propos_footer">
                    <div class="footer-widget ">
                        <div class="footer-title">
                            <div class="footer_title_border">A propos</div>
                        </div>
                        <ul class="list-unstyled">
                            <li><a href="#">Conditions générales de vente et d'utilisation</a></li>
                            <li><a href="#">Politique de confidentialité et de respect de la vie privée</a></li>
                            <li><a href="#">Mentions légales</a></li>
                        </ul>
                    </div>
                </div>
                <div id="questions_footer">
                    <div class="footer-widget">
                        <div class="footer-title">
                            <div class="footer_title_border">Questions fréquentes</div>
                        </div>
                        <ul class="list-unstyled">
                            <li><a href="#">Comment poster une service ou objet ?</a></li>
                            <li><a href="#">Comment annuler une demande de location ?</a></li>
                            <li><a href="#">Peut on payer sur place?</a></li>
                            <li><a href="#">Mon objet est il assuré par votre site?</a></li>
                        </ul>
                    </div>
                </div>
                <div id="dev_footer">
                    <div class="footer-widget ">
                        <div class="footer-title">
                            <div class="footer_title_border">Développement durable</div>
                        </div>
                        <ul class="list-unstyled">
                            <li><a href="#">Qui sommes nous?</a></li>
                            <li><a href="#">Livre blanc de la consommation colaborative</a></li>
                        </ul>
                    </div>
                </div>
                <div id="contact_footer">
                    <div class="footer-widget ">
                        <div class="footer-title">
                            <div class="footer_title_border">Contact</div>
                        </div>
                        <ul class="list-unstyled">
                            <li><a href="#">Nos partenaires</a></li>
                            <li><a href="#">Espace presse</a></li>
                            <li><a href="#">Nous contacter</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <div class="d-flex flex-row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center ">
                <div class="tiny-footer">
                    <p>Copyright © All Rights Reserved 2020 | Vopus</p>
                </div>
            </div>
        </div>
    </div>
</div>