<!doctype html>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/lib/Bootstrap4/bootstrap.min.css">
    <!-- Page CSS -->  
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
      <!--Owl Carousel CSS -->
      <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
      <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <!-- Font awesome CSS -->
    <link rel="stylesheet" href="assets/lib/fontawesome-free-5.11.2-web/css/all.min.css">
      <!-- DatePicker Datedropper custom theme CSS -->
      <link rel="stylesheet" href="assets/css/datepicker2-vopus.css">


    <title>
        <?php if (isset($title)) : ?>
                <?php echo $title; ?>
            <?php  else : ?>
                Vopus
            <?php endif ?>
    </title>
  </head>