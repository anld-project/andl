<header>
    <section>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">
                <div class="logo_integration">
                   <img src="assets/images/logos/logo_vopus_simple.gif">
                </div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item" id="espPro">
                        <a class="nav-link" href="list_propositions.php">Toutes les propositions</a>
                    </li>
                    <li class="nav-item" id="espCon">
                        <a class="nav-link" href="#">Le concept</a>
                    </li>
                    <li class="nav-item">
                        <a href="login.php">
                            <button type="button" id="btnLogin" class="btn btn-dark">Se connecter</button>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="registration.php">
                            <button type="button" id="btnRegistration" class="btn btn-outline-dark">S'inscrire</button>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </section>
</header>