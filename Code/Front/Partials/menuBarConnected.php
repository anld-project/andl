<header>
<?php
    //session_start();
    $_SESSION['onglet'] = 0;
    
?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" >
        <a class="navbar-brand" href = "index_user.php" >
            <div class="logo_integration">
                <img src="assets/images/logos/logo_vopus_simple.gif">
            </div>     
       </a >
        <button class="navbar-toggler" type = "button" data-toggle = "collapse" data-target = "#navbarNavDropdown" aria-controls = "navbarNavDropdown" aria-expanded = "false" aria-label = "Toggle navigation" >
            <span class="navbar-toggler-icon" ></span >
        </button >
        <div class="collapse navbar-collapse" id = "navbarNavDropdown" >
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0" >
                <li class="nav-item" id = "espPro" >
                    <a class="nav-link" href = "list_propositions.php" > Toutes les propositions </a >
                </li >
                <li class="nav-item" id = "espCon" >
                    <a class="nav-link" href = "#" > Le concept </a >
                </li >
                <div id = "menuIcons" >
                    <li class="nav-item" >
                        <a class="nav-link" href = "chatroom.php" ><i class="fas fa-envelope" ></i ></a >
                    </li >
                    <li class="nav-item dropdown" >
                        <a class="nav-link dropdown-toggle" href = "#" id = "navbarDropdownMenuLink" role = "button" data-toggle = "dropdown" aria-haspopup = "true" aria-expanded = "false" >
                            <i class="fas fa-user-circle" ></i >
                        </a >
                        <div class="dropdown-menu" aria-labelledby = "navbarDropdownMenuLink" >
                            <a class="dropdown-item" href = "profile.php" > Mon profil </a >
                            <a class="dropdown-item" href = "userspace.php" > Mes annonces </a >
                            <a class="dropdown-item" href = "my_deals.php" > Mes DEALS </a >
                            <a class="dropdown-item" href = "logout.php" > Se déconnecter </a >
                        </div >
                    </li >
                    <li class="nav-item" >
                        <a class="nav-link" href = "#" ><i class="fas fa-question-circle" ></i ></a >
                    </li >
                </div >
            </ul >
        </div >
    </nav >
</header >
