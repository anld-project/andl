<?php

require_once '../functions.php';

if ($_POST && !empty($_POST)) {

    if  (
            !empty($_POST['firstname'])
            && !empty($_POST['mobilenumber'])
            && !empty($_POST['email'])
            && !empty($_POST['password'])
        )
        {
            registerUserAll(
                    $_POST['email'],
                    $_POST['password'],
                    $_POST['firstname'],
                    $_POST['mobilenumber'],
                    $_POST['lastName'],
                    $_POST['addressNumber'],
                    $_POST['addressStreet'],
                    $_POST['addressPostalCode'],
                    $_POST['addressCity'],
                    $_POST['kmVisibility'],
                    $_POST['verified'],
                    $_POST['banned'],
                    $_POST['dealCounter'],
                    $_POST['userPictureLink']
                );
        } 
        else 
        { 
            redirect('login.php?error=field-empty');
        }

}

?>

<?php include('../Partials/head.php'); ?>

<body>

<?php include('../Partials/menuBarNoConnected.php'); ?>

<main class="mainIns" id="mainPicture1">

    <div class="container d-flex justify-content-center" class="rounded">
        <div class="card" style="width:400px" >
            <article class="card-body mx-auto" style="max-width: 500px;">
                <h4 class="card-title pb-2 text-center">Inscription</h4>
                <form method="post">
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                        </div>
                        <input name="firstname" id="firstname" class="form-control" placeholder="Prénom" type="text">
                    </div> <!-- form-group// -->
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input name="email" id="email" class="form-control" placeholder="Adresse mail" type="email">
                    </div> <!-- form-group// -->
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                        </div>

                        <input name="mobilenumber" id="mobilenumber" class="form-control" placeholder="Numéro de téléphone" type="text">
                    </div> <!-- form-group// -->

                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="password" id="password" class="form-control" placeholder="Mot de passe"
                               type="password">
                    </div> <!-- form-group// -->
                    
                    //--- lastName
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="lastName" id="lastName" class="form-control" placeholder="nom famille"
                               type="text">
                    </div>
                    
                    //--- addressNumber
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="addressNumber" id="addressNumber" class="form-control" placeholder="Adresse : Numero"
                               type="text">
                    </div>
                    
                    //--- addressStreet
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="addressStreet" id="addressStreet" class="form-control" placeholder="Adresse : rue"
                               type="text">
                    </div>
                    
                    //--- addressPostalCode
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="addressPostalCode" id="addressPostalCode" class="form-control" placeholder="adresse : code postal"
                               type="text">
                    </div>
                    
                    //--- addressCity
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="addressCity" id="addressCity" class="form-control" placeholder="adresse : ville"
                               type="text">
                    </div>
                    
                    //--- kmVisibility
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="kmVisibility" id="kmVisibility" class="form-control" placeholder="Perimetre de visibilité (km)"
                               type="text">
                    </div>
                    
                    //--- verified
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="verified" id="verified" class="form-control" placeholder="Profile verifie"
                               type="text">
                    </div>
                    
                    //--- banned
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="banned" id="banned" class="form-control" placeholder="Profile banné"
                               type="text">
                    </div>
                    
                    //--- dealCounter
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="dealCounter" id="dealCounter" class="form-control" placeholder="Compteur de transactions ok"
                               type="text">
                    </div>
                    
                    //--- userPictureLink
                    
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="userPictureLink" id="userPictureLink" class="form-control" placeholder="Lien image"
                               type="text">
                    </div>
                    
                    //=== SUBMIT
                    
                    <div class="form-group">
                        <a href="utilisateur_connu.html">
                            <button type="submit" class="btn btn-primary btn-block">S'inscrire</button>
                        </a>
                    </div> <!-- form-group// -->
                    <p id="haveAnAccount" class="text-center">Vous avez déja un compte? <a href="login.php">Connexion</a></p>
                </form>
            </article>
        </div>
    </div>

</main>
</body>

<?php include('../Partials/scriptLinksBootstrap.php'); ?>