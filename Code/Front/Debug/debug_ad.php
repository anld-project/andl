<?php
include('../Partials/head.php'); ?>

<head>
<style>
    
* {
  box-sizing: border-box;
    padding: 0;
    margin: 0;
}
    
#alned_mainPicture1{
    background-image: url("../assets/images/main_photo1.png");
    background-size:contain;
    min-height: 100vh;
}

.container-main{
    display:flex;
    flex-direction: column;
    justify-content: space-between;
    width:100%;
    height: 3000px;
    margin:auto;
}

#alned_greatBlock1{
    width: 100%;
    height: 4500px;
    display: flex;
}
    
    
#id-img { 
          height: 150px;
          width: 150px;
          margin-left: 60px;
          border: 3px solid white;
        }
    
#id-prenom { 
            margin-left: 60px;
            font-size: 28px;
         }
    
#alned_parentBlock1{
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content:space-around;
    padding: 20px;
}
    
#alned_littleChildBlock1{
    width: 800px;
    height: 400px;
    margin-bottom:50px;
    
}
    
#alned_childBlock2{
    display:flex;
    justify-content: center;
    align-items: center;
    width: 800px;
    height: 400px;
    background-color: rgba(255, 255, 255, 0.65);
    border-radius: 40px;
}

#alned_id_title1{
    color: rgb(204, 35, 160);
    font-weight: 700;
    font-size: 28px;
    margin-bottom: 50px;
}
    
#alned_winTitle12{
    font-size: 12px;
}

#alned_obj{
    display:flex;
    flex-direction: column;
    align-items:baseline;
    margin-top: 50px;
    padding-left: 20px;
}
    
#alned_id_content{
    display:flex;
    flex-direction:row;
    justify-content: space-around;
    align-items: center;
}
    
#alned_id_text1{
    color:black;
    font-size: 14px;
}
    
#alned_id_text2{
    color:black;
    font-size: 16px;
    font-weight: 500;
}
    
</style>
</head>

<body>
   
    <?php 
        require_once('../functions.php');
        require_once('../assets/class/daba.class.php');
    
        $dab = new Daba(ORDER_DESC);
        $nb_ad = $dab->getNbad();
    ?>
    
    <main class="container-main" id="alned_mainPicture1">
       
        <section id="alned_greatBlock1">
           
            <div id="alned_parentBlock1">
               
                <?php for($m=0; $m < $nb_ad; $m++): ?>
               
                <div id="alned_childBlock2">
                   
                    <div id="alned_littleChildBlock1">
 				
                        <?php $dab->NextAd(); ?>
                      
                        <div id="alned_id_content">
                            <div style="flex-grow: 1">
                              <img id="id-img" src="<?php echo "../assets/images/img_users/".$dab->getUse()->id."/".$dab->getUse()->userPictureLink ?>" >
                              <p id="id-prenom"><?php echo $dab->getUse()->firstName ?></p>
                            </div>
                            <div id="alned_obj"  style="flex-grow: 9">
                                
                                 <h3 id="alned_id_title1">
                                 <?php if ($dab->getAd()->needBidAttribute == 'NEED') echo "Je cherche"; else echo "Je vend";
                                   echo " ".$dab->getObj()->label; ?>
                                 </h3>
                                 
                                <div>
                                    <div id="alned_id_text1">Type de transaction : <?php echo $dab->getAd()->leasingSaleAttribute; ?></div>
                                     <div id="alned_id_text2"><br></div>
                                     <div id="alned_id_text2"><?php echo $dab->getAd()->introductionText; ?></div>
                                     <div id="alned_id_text2"><br></div>
                                     <div id="alned_id_text2"><?php echo "Depuis le ".$dab->getAd()->creationDate; ?></div>
                                     <div id="alned_id_text2"><br></div>
                                     
                                     <?php if($dab->getShe()->startDate != '') :?>
                                     <div id="alned_id_text2">Entre <?php echo str_replace("00:00:00", "", $dab->getShe()->startDate ); ?> et <?php echo str_replace("00:00:00", "", $dab->getShe()->endDate ); ?> :</div>
                                     <?php endif; ?>
                                     
                                     <?php
                                       $nb_date = CountDate($dab->getShe());
                                       $seq = GetSheduleDate($dab->getShe());
                                       if ($nb_date != 0):
                                     ?>
                                     
                                     <?php for($d=0; $d < $nb_date; $d++): ?>
                                         <?php $date = NextDate($seq);?>
                                              <div id="alned_id_text2">le <?php echo $date->dateHour; echo ", ".$date->hourDuration."heures"; ?> </div>
                                     <?php endfor; ?>
                                     
                                     <?php endif; ?>
                                      
                                     <div id="alned_id_text2"><br></div>
                                     <div id="alned_id_text2">le prix : <?php echo $dab->getAd()->pricing; ?>€ <?php if($dab->getAd()->leasingSaleAttribute == 'LEASING') echo "(par heure)"; ?></div>
                                     
                                     
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>
                
                <?php endfor; ?>
                
            </div>
        </section>
    </main>

<?php include('../Partials/scriptLinksBootstrap.php'); ?>