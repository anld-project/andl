<?php
include('../Partials/head.php'); ?>

<head>
<style>
    
* {
  box-sizing: border-box;
    padding: 0;
    margin: 0;
}
    
#alned_mainPicture1{
    background-image: url("../assets/images/main_photo1.png");
    background-size:contain;
    min-height: 100vh;
}

.container-main{
    display:flex;
    flex-direction: column;
    justify-content: space-between;
    width:100%;
    height: 10000px;
    margin:auto;
}

#alned_greatBlock1{
    width: 100%;
    height: 7000px;
    display: flex;
}
    
    
#id-img { 
          height: 150px;
          width: 220px;
          margin-left: 60px;
          border: 3px solid white;
        }
    
#alned_parentBlock1{
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
}
    
#alned_littleChildBlock1{
    width: 350px;
    height: 1000px;
    margin-bottom:100px;
    
}
    
#alned_childBlock2{
    display:flex;
    justify-content: center;
    align-items: center;
    width: 400px;
    height: 1100px;
    background-color: rgba(255, 255, 255, 0.65);
    border-radius: 40px;
}

#alned_winTitle1{
    text-align: center;
    color: rgb(204, 35, 160);
    font-weight: 700;
    font-size: 28px;
}
    
#alned_obj{
    display:flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    font-size: 12px;

}
    
#alned_id_obser{
    color:red;

}
    
</style>
</head>

<body>
<?php 
    require_once('../functions.php');
    
    error_reporting(E_ALL);
    
    if(!empty($_POST['choix']))
    {
        StoreMoment($_POST['choix'],0);
        redirect("debug_moment_store.php");
    }
    
    if(!empty($_GET))
    {
        DeleteMoment();
    }
    
    unset($_POST);
    unset($_GET);
    
    //recherche des moments dans database
    
    $nb_mom = CountMoments();
    $req = GetMoments();   
    
?>

    <main class="container-main" id="alned_mainPicture1">
       
        <section id="alned_greatBlock1">
           
            <div id="alned_parentBlock1">
               
               
               
                <?php for($m=0; $m<$nb_mom; $m++): ?>
               
               
                <div id="alned_childBlock2">
                   
                    <div id="alned_littleChildBlock1">
 				
                        <?php 
                                $mom = NextMoment($req);
                                $idob = GetObjects($mom);
                    
                                if ($mom->title == "OBJECT")
                                {$impath = "../assets/images/img_moments/objet.png";}
                                elseif($mom->title == "SERVICE")
                                {$impath = "../assets/images/img_moments/service.png";}
                                else {$impath = "../".$mom->iconLink;}
                        ?>
                       
                        <img id="id-img" src="<?php echo $impath; ?>">
                        

                        <div id="alned_winTitle1">
                            <article>
                                <?php 
                                echo "$mom->title";
                                define('NBX',100);
                                ?> 
                            </article>
                        </div>
                        
                        
                        
                        <form method="POST" action="">
                         
                        <div id="alned_obj">
                        
                        <div>
                            <?php for($i=0,$f=0; $i<NBX && $i<count($idob); $i++): ?>
                            <?php $obj = NextObject($idob,$i,ATR_OBJECT); if ($obj->label != ''): ?>
                            <?php if ($f++ == 0): ?><h3 id="alned_id_obser">objet</h3><?php endif; ?>

                                <input type="checkbox" name="choix[]" id="" value="<?php echo (string)$mom->id."-".(string)$obj->id; ?>" ><strong><?php echo $obj->label; ?></strong><br>

                            <?php endif; ?>
                            <?php endfor; ?>
                                
                                
                        </div>

                        <div>                            
                            
                            <?php for($i=0,$f=0; $i<NBX && $i<count($idob); $i++): ?>
                            <?php $obj = NextObject($idob,$i,ATR_SERVICE); if ($obj->label != ''): ?>
                                <?php if ($f++ == 0): ?><h3 id="alned_id_obser">service</h3><?php endif; ?>

                                <input type="checkbox" name="choix[]" id="" value="<?php echo (string)$mom->id."-".(string)$obj->id; ?>" ><strong><?php echo $obj->label; ?></strong><br>

                            <?php endif; ?>
                            <?php endfor; ?>
                            
                            
                        </div>

                        </div>
                        
                        <input type="submit" value="Create new moment">
                        </form> 
                    
                        <form method="GET" action="debug_moment_delete.php">
                            <input type="submit" name="mom" value="<?php echo "Delete ".$mom->id; ?>">
                        </form>
                                            
                        
                        
                    </div>

                </div>
                
                <?php endfor; ?>
              
                
                
            </div>
        </section>
    </main>

<?php include('../Partials/scriptLinksBootstrap.php'); ?>