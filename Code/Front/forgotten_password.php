<?php
$title = "Mot de passe oublié";
include('Partials/head.php'); ?>

<body>
<?php include('Partials/menuBarNoConnected.php'); ?>

<main id="mainPicture1">
    <div class="container h-100" style="padding-top: 6rem !important;">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="card" >
                <h6 class="card-header">
                    <strong>Vous avez oublié votre mot de passe</strong>
                </h6>
                <div class="card-body">


                    <!-- Si l'adresse mail n'est pas présente dans la base de donnée : -->

                    <?php if ($_GET && $_GET['auth'] == 'fail'): ?>
                        <div class="alert alert-danger" role="alert">
                            Cette adresse mail n'existe pas...<br> Veuillez recommencer
                        </div>
                    <?php endif; ?>


                    <!-- Si l'adresse mail est présente dans la base de donnée : -->

                    <?php if ($_GET && $_GET['auth'] == 'fail'): ?>
                        <div class="alert alert-success" role="alert">
                            Nous venons de vous envoyer les instructions afin de réinitialiser votre mot de passe
                        </div>
                    <?php endif; ?>



                    <form data-toggle="validator" role="form" method="post">
                        <input type="hidden" class="hide" id="#" name="#"
                               value="#">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group">
                                        <input name="email" id="email" type="text" class="form-control"
                                               placeholder="Adresse email" style="color:#d0d3d4" pattern=".{4,}" title="" required="">
                                    </div>
                                    <div class="help-block with-errors text-danger"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row" class="passwordForm">
                            <div class="col-md-12 pt-3">
                                <input type="hidden" name="redirect" value="#">
                                <input type="submit" class="btn btn-primary btn-lg btn-block" value="Envoyer"
                                       name="submit">
                            </div>
                        </div>
                    </form>
                    <div class="clear"></div>
                    <div class="paddLog" style="color:white">
                        Pas encore membre?
                        <a href="inscription.php"> Inscription</a><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<?php include('Partials/scriptLinksBootstrap.php'); ?>
