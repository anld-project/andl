<?php

require_once('functions.php');

if ($_POST && !empty($_POST)) {
    if (!empty($_POST['email']) && !empty($_POST['password'])) {
        connectUser($_POST['email'], $_POST['password']);
    } else {
        redirect('login.php?error=field-empty');
    }
}
?>

<?php
$title = "Page de connexion";
include('Partials/head.php'); ?>


<body>
<?php include('Partials/menuBarNoConnected.php'); ?>
<main id="mainPicture1">
    <div class="container h-100 pt-5">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="card" >
                <h6 class="card-header"><strong>Connectez-vous à votre compte</strong>
                </h6>
                <div class="card-body">
                   
				
				    <?php if ($_GET && $_GET['auth'] == 'fail'): ?>
                        <div class="alert alert-danger" role="alert">
                            Email et / ou mot de passe incorrect.
                        </div>
                    <?php endif; ?>					
                    
                    
                    <form data-toggle="validator" role="form" method="post">
                        <input type="hidden" class="hide" id="csrf_token" name="csrf_token"
                               value="C8nPqbqTxzcML7Hw0jLRu41ry5b9a10a0e2bc2">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="input-group">

                                        <input name="email" id="email" type="text" class="form-control"
                                            placeholder="Adresse email" style="color:#d0d3d4" pattern=".{4,}" title="" required="">
                                    </div>
                                    <div class="help-block with-errors text-danger"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Mot de passe</label>
                                    <div class="input-group">

                                        <input name="password" id="password" type="password"
                                               class="form-control" placeholder="Password" style="color:#d0d3d4" pattern=".{4,}" title="" required="">
                                    </div>
                                    <div class="help-block with-errors text-danger"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row" class="passwordForm">
                            <div class="col-md-12 pt-3">
                                <input type="hidden" name="redirect" value="">
                                    <input type="submit" class="btn btn-primary btn-lg btn-block" value="Se connecter"
                                           name="submit">
                            </div>
                        </div>
                    </form>
                    <div class="clear"></div>
                    <div class="paddLog" style="color:white">
                        Pas encore membre?
                        <a href="registration.php"> Inscription</a><br>
                    </div>
                    <div class="paddLog">
                        <a href="#">Oubli de votre mot de passe?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include('Partials/scriptLinksBootstrap.php'); ?>

