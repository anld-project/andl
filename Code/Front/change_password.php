<?php
$title = "Changer son mot de passe";
include('Partials/head.php'); ?>

<body>
<?php include('Partials/menuBarNoConnected.php'); ?>

<main id="mainPicture1">
    <div class="container h-100" style="padding-top: 6rem !important;">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="card" >
                <h6 class="card-header"><strong>Renseignez un nouveau mot de passe</strong>
                </h6>
                <div class="card-body">

                    
                    
                    <!-- Si les 2 mots de passe saisis sont différents :  -->

                    <?php if ($_GET && $_GET['auth'] == 'fail'): ?>
                        <div class="alert alert-danger" role="alert">
                            Vos saisies sont différentes
                        </div>
                    <?php endif; ?>


                    
                    
                    <!-- Si les 2 mots de passe saisis sont identiques : -->
                    <!-- Cette seconde condition sera épaulée à l'avenir par un pop-up  -->

                    <?php if ($_GET && $_GET['auth'] == 'fail'): ?>
                        <div class="alert alert-success" role="alert">
                            Parfait! Vous venez de changer votre mot de passe !<br>
                            Vous pouvez désormais vous connecter !
                        </div>
                    <?php endif; ?>
                    
                    
                    
                    
                    <form data-toggle="validator" role="form" method="post">
                        <input type="hidden" class="hide" id="#" name="#" value="#">
                 
                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Votre nouveau mot de passe</label>
                                    <div class="input-group">

                                        <input name="password" id="password" type="password"
                                               class="form-control" placeholder="Password" style="color:#d0d3d4" pattern=".{4,}" title="" required="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Répéter votre mot de passe</label>
                                    <div class="input-group">

                                        <input name="password" id="password" type="password"
                                               class="form-control" placeholder="Password" style="color:#d0d3d4" pattern=".{4,}" title="" required="">
                                    </div>
                                    <div class="help-block with-errors text-danger"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row" class="passwordForm">
                            <div class="col-md-12 pt-3">
                                <input type="hidden" name="redirect" value="">
                                <input type="submit" class="btn btn-primary btn-lg btn-block" value="Valider"
                                       name="submit">
                            </div>
                        </div>
                    </form>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include('Partials/scriptLinksBootstrap.php'); ?>
