<?php

require_once('functions.php');
require_once('./assets/class/user.class.php');

header('Content-Type: application/json');

$idUser = $_SESSION['idUser'];

if ($_POST) {
    
    $status = "";

    $tabs = $_POST['choix'];
    foreach( $tabs as $t ){
        $status = $status.$t;
    }

    file_put_contents('./assets/images/img_users/'.$idUser.'/user.div', $status);

    $img = preg_split('/&quot;/', $status);

    $data = $img[1];

    if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
        $data = substr($data, strpos($data, ',') + 1);
        $type = strtolower($type[1]); // jpg, png, gif

        if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
            throw new \Exception('invalid image type');
        }

        $data = base64_decode($data);
        file_put_contents('./assets/images/img_users/'.$idUser.'/'."user.{$type}", $data);
    }
    
} else $status = "*** POST ABSENT ***<br>";

echo json_encode($status);

?>
