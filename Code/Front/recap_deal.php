<?php

require_once('functions.php');
require_once('./assets/class/user.class.php');
include('partials/head.php');

        
    $deal_text='ma proposition...'; $option_heure=0; $option_budget=0; $option_duration=0; $priceAttribute='Heure';

    if ($_GET)
    {

    if(isset($_GET['deal_text']) AND !empty($_GET['deal_text'])) $deal_text = $_GET['deal_text'];

    if(isset($_GET['option_jour']) AND !empty($_GET['option_jour'])) $priceAttribute='Jour';
    if(isset($_GET['option_global']) AND !empty($_GET['option_global'])) $priceAttribute='Global';

    if(isset($_GET['option_budget']) AND !empty($_GET['option_budget'])) $option_budget = $_GET['option_budget'];

    if(isset($_GET['option_duration']) AND !empty($_GET['option_duration'])) $option_duration = $_GET['option_duration'];

    }

    $_SESSION['deal_text'] = $deal_text;
    $_SESSION['priceAttribute'] = $priceAttribute;
    $_SESSION['option_budget'] = $option_budget;
    $_SESSION['option_duration'] = $option_duration;

    //extraction des infos annonce slider page acceuil
        
    $idAdLastProp = $_SESSION['idAdLastProp'];
    $ad = GetAdId($idAdLastProp);
    $adobj = GetIdObject(GetAdMoment($ad)->id_object);

    //extraction des infos user courant

    $idUser = $_SESSION['idUser'];
    $user = new user($idUser);
    $obj = $user->NextUser();

?>

<body>

<?php include('partials/menuBarConnected.php'); ?>

<div class="mainRecapDeal">
    <div id="mainRecapDealChild">
    <div class="titleRecapDeal">
        <div>
           <?php if ($ad->needBidAttribute == 'NEED'): ?>
                <article style="color:rgba(9, 204, 35, 160);" >Recapitulatif de votre offre...</article>
           <?php else: ?>
                <article style="color:rgb(204, 35, 160);">Recapitulatif de votre besoin...</article>
           <?php endif; ?>
        </div>
    </div>
    <div class="mainBlockRecapDeal">
        <div id="photoRecapDeal">
              <div id="photoDealProp">
                    <img src="<?php echo $user->getUsePortrait(); ?>">
               </div>
                
            
                  <div id="infoProfilDealProp">
                    <div id="first-name-profile">
                        <h4><?php echo $obj->firstName; ?></h4>
                    </div>
                <div id="city-name-profile5">
                    <h4>
                        <i class="fas fa-map-marker-alt"></i>
                        <?php echo $obj->addressCity; ?>
                    </h4>
                </div>
                <div class="star-rating-profile">
                    <div>
                        <img src="assets/images/star-rating.png" width="30px;">
                    </div>
                    <div>
                        <article>4/5</article>
                    </div>
                </div>
               </div>
        </div>
        <div class="photosAndBubble">
            <div class="photosCentralBlock">
                <section class="titleCentralBlockDeal">
                    <article>Vos photos :</article>
                </section>
                <section class="littleBlockDealPhotos">
                        <div id="little-modify-photo">
                            <div id="little-modify-photo2">
                                <img src="<?php echo $user->getUsePic().'/pic.png'; ?>">
                                     <!--div id="croix-photo">
                                        <img src="assets/images/red-cross-photo.png" width="10px">
                                    </div-->
                             </div>
                        </div>
                </section>
            </div>
            <div class="mainBubbleRecap">
               <section class="titleCentralBlockDeal">
                    <article>Description :</article> 
                </section>
                <div class="bubble">
                    <div class="bubble-text">
                        <p><?php echo "$deal_text"; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="mainPrevisionDeal">
          <section class="titleCentralBlockDeal">
                <article>Prévision :</article>  
          </section>
            <section id="previsionDeal">
                <div id="previsionDealText">
                    <section>
                        <strong><?php echo budgetPriceConvert($option_budget); ?></strong><strong> € </strong>
                        <?php if($priceAttribute == 'Global'): ?>
                            <span>au total </span>
                        <?php else: ?>
                            <span>par <?php echo $priceAttribute; ?> </span>
                        <?php endif; ?>
                        
                        <span>pour un temps estimé </span>
                        
                        <?php if($option_duration == 0): ?>
                            <span>inconnu...</span><br>
                        <?php else: ?>
                            <span>de <?php echo timeListConvert($option_duration); ?></span><br>
                        
                            <?php if($priceAttribute != 'Global'): ?>
                                <span>Soit un total de </span><strong><?php echo priceEstimate($option_budget,$priceAttribute,$option_duration); ?></strong><strong> €</strong><br>
                            <?php endif; ?>
                        
                        <?php endif; ?>
                    </section>
                </div>
            </section>
        </div>
        <div id="btnRecapDeal">
            <section>            
                <form action="/deal_formulation.php">
                    <input type="submit" class="btn btn-dark" value="Retour">
                </form>
            </section>
            <section>
                <form action="/recap_deal_action.php">
                    <input type="submit" class="btn btn-white" value="Valider">
                </form>
            </section>            
        </div>     
   
        
        </div>
        
        </div>
</div>

<?php 
    include('partials/footer.php');
    include('partials/scriptLinksBootstrap.php'); 
?>