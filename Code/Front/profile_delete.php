<?php
    require_once('functions.php');
    $idUser = $_SESSION['idUser'];

    deleteUser($idUser);

    redirectToLogin();
?>
