<?php
require_once('functions.php');
$title = "Liste de moments";
include('Partials/head.php');

define('NB_MAX_VIGNETTE', 50);  //nombre de vignettes max affichées

$momid = 0;
if (isset($_GET['momid']) AND !empty($_GET['momid'])) $momid = $_GET['momid'];
$mom = getMomentId($momid);
?>

<body>
<?php include('Partials/menuBarConnected.php'); ?>

<div class="main-disposition1">

    <!-- Début du changement de l'image selon le moment choisi -->

    <div id="top-photo-of-moments" style=background-image:url('assets/images/banners_moments/<?php echo getMomentBanner($mom->id); ?>');>
        <span id="title-photo-moments"><?php echo "$mom->title"; ?></span>
    </div>

    <!-- Fin du changement de l'image selon le moment choisi -->


        <div id="central-block-moments">
            <div id="central-child-block">
                <div id="descrip-moments">
                    <section id="descrip-child-moments">
                        <h3>Postez les annonces dont vous avez besoin pour l'organisation
                            de <?php echo "$mom->title"; ?></h3>
                    </section>
                </div>
                <div class="title-moments4">
                    <h4>Services</h4>
                </div>
                <div id="block-moments-services">


                    <div id="block-child-moments-services">


                        <!-- Début Vignette moment-->
                        <?php
                        $idob = GetObjects($mom);
                        for ($i = 0, $j = 0; $i < count($idob); $i++):
                            $obj = NextObject($idob, $i, ATR_NONE);

                            if (($obj->attribut == ATR_SERVICE) && ($j < NB_MAX_VIGNETTE)):
                                ?>

                                <section class="thumbnail-moments">
                                    <div class="thumbnail-moments2" tmsel="0" tmval="<?php echo (string)$mom->id . "-" . (string)$obj->id; ?>">
                                        <div id="moments-check3">
                                            <div class="form-check">
                                                   <article>
                                                       <strong><?php echo $obj->label; ?></strong>
                                                   </article>
                                            </div>
                                        </div>
                                     </div>
                                </section>

                                <?php $j++; endif; ?>
                        <?php endfor; ?>

                        <!-- Fin Vignette moment-->


                    </div>
                </div>
                <div class="title-moments4">
                    <h4>Locations</h4>
                </div>
                <div id="block-moments-locations">

                    <div id="block-child-moments-locations">

                        <!-- Début Vignette moment-->

                        <?php
                        $idob = GetObjects($mom);
                        for ($i = 0, $j = 0; $i < count($idob); $i++):
                            $obj = NextObject($idob, $i, ATR_NONE);

                            if (($obj->attribut == ATR_OBJECT) && ($j < NB_MAX_VIGNETTE)):
                                ?>

                                <section class="thumbnail-moments">
                                    <div class="thumbnail-moments2" tmsel="0" tmval="<?php echo (string)$mom->id . "-" . (string)$obj->id; ?>">
                                        <div id="moments-check3">
                                            <div class="form-check pl-0">
                                                <article>
                                                    <strong><?php echo $obj->label; ?></strong>
                                                </article>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <?php $j++; endif; ?>
                        <?php endfor; ?>

                        <!-- Fin Vignette moment-->


                    </div>
                </div>
            </div>
            <div id="block-post-moments">
                <div id="block-child-post-moment">
                    <div class="nomber-moments">
                        <div class="title-number-moments">
                            <h3><?php echo "$mom->title"; ?></h3>
                        </div>
                        <div class="title-number-moments">
                            <h1>0</h1>
                        </div>
                        <div class="title-number-moments">
                            <article>propositions sélectionnées</article>
                        </div>
                    </div>
                    <div id="city-post-moments">
                        <div class="title-number-moments">
                            <article>Où en avez vous besoin?</article>
                        </div>
                        <div class="block-field-post-city">
                            <div id="field-post-moments-city" class="input-group pt-4">
                                <input class="cityPostMoments" type="text" class="form-control" placeholder="Ville"
                                       aria-label="" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                    <div class="block-post-date1">
                        <div id="need-date-post">
                            <article>En avez vous besoin pour une date précise?</article>
                        </div>
                        <section class="btnBoolPost">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="optradio">Oui
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="optradio">Non
                                </label>
                            </div>
                        </section>
                        <div class="main-date-moments">
                            <div id="date-moments-post1">
                                <div class="form-group row">
                                    <div class="check-date-moment">
                                        <input type="text" data-datedropper data-dd-format="d-m-Y" data-dd-lang="fr"
                                               data-dd-large="true" data-dd-large-default="false"
                                               data-dd-translate-mode="true" data-dd-event-selector="click"
                                               data-dd-theme="datepicker2-vopus" name="birthdayDate" value="02-01-2020"
                                               id="moment-date-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="btnPostMoments">
                            <input type="submit" class="btn btn-primary" value="Poster mes demandes">
                        </div>

                    </div>
                </div>
            </div>

        </div>


</div>
<div>
    <?php
    include('Partials/footer.php');
    include('Partials/scriptLinksBootstrap.php');
    ?>
</div>
        
<script src="assets/JavaScript/list_moments.js"></script>

</body>