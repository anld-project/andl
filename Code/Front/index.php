<?php

require_once('functions.php');
require_once('./assets/class/daba.class.php');

$title = "Page d'acceuil";
include('Partials/head.php');

?>

<body>
<?php include('Partials/menuBarNoConnected.php'); ?>


<main id="mainPicture1">
    <section id="greatBlock1">
        <div id="accroche_logo_index">
            <div>
                <h5>Des économies</h5>
            </div>
            <div>
                <h5>à partager !</h5>
            </div>
        </div>
        <div id="parentBlock1">
            <div id="childBlock2">
                <div id="littleChildBlock1">
                    <div id="winTitle1">
                        <article>Je recherche</article>
                    </div>
                    <section class="btnRadioAccueil">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                            <label class="form-check-label" for="exampleRadios1">
                                <article>Une location</article>
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                            <label class="form-check-label" for="exampleRadios2">
                                <article>Un service</article>
                            </label>
                        </div>
                    </section>
                    <div id="winTitle12" class="pt-3 pb-2"><strong>Précisez votre besoin</strong></div>
                    <div class="input-group">
                            <textarea class="form-control" placeholder="Bonjour, je recherche une tondeuse à gazon"
                                      aria-label=""></textarea>
                    </div>

                    <div id="fieldCity" class="input-group pt-4">
                        <input class="cityFormHome" type="text" class="form-control" placeholder="Ville ou code postal"
                               aria-label="" aria-describedby="basic-addon1">
                    </div>
                    <div id="btnSearchHome">

                        <button type="button">Je recherche</button>
                    </div>
                </div>
            </div>

            <div id="childBlock3">

                <div id="littleChildBlock2">
                    <div id="winTitle2">
                        <article>Je propose</article>
                    </div>
                    <section class="btnRadioAccueil">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3">
                            <label class="form-check-label" for="exampleRadios3">
                                <article>Une location</article>
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios4" value="option4">
                            <label class="form-check-label" for="exampleRadios4">
                                <article>Un service</article>
                            </label>
                        </div>
                    </section>
                    <div id="winTitle12" class="pt-3 pb-2"><strong>Précisez votre offre</strong></div>
                    <div class="input-group">
                            <textarea class="form-control" placeholder="Bonjour, je donne des cours de piano"
                                      aria-label=""></textarea>
                    </div>
                    <div id="fieldCity" class="input-group pt-4">
                        <input class="cityFormHome" type="text" class="form-control" placeholder="Ville ou code postal"
                               aria-label="" aria-describedby="basic-addon1">
                    </div>
                    <div id="btnSearchHome">

                        <button type="button">Je propose</button>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>

<!-- Slider : moments of life -->

<?php
    
    $nb_mom = CountMoments();  //recherche des moments dans database
    $req = GetMoments();   

?>
    
    
<div class="title12">Nos Moments de Vie</div>
<div class="parent">
    <div class="owl-carousel owl-theme">

        <?php for($m=0; $m<$nb_mom; $m++): ?>
        <?php 
            $mom = NextMoment($req);
            $idob = GetObjects($mom);
            if ($mom->referent == MOMENT_REFERENT):
        ?>
               
        <div class="items">
                <a href="login.php">
                <div class="item__image">
                    <img src="<?php echo $mom->iconLink; ?>" alt="Déménagement">
                </div>
            </a>
            <div class="item__body">
                <div class="item__title"><?php echo "$mom->title"; ?></div>
            </div>
        </div>

        <?php endif; ?>
        <?php endfor; ?>

    </div>
</div>

<!-- Fin slider moments de vie -->

<!-- Début section développement durable -->

<section class="three_central container-fluid">
    <div class="three_central_child">
        <section class="logo_dev_three">
            <img src="assets/images/logos/logo-vopus-accroche.gif">
        </section>

        <section id="title_three_dev">
            <article>Agit pour le développement durable</article>
        </section>
        <section class="great_block_central_three d-flex flex-row justify-content-around ">

            <!--Début : vignette 1er pillier du développement durable -->

            <div class="dev-central-block">
                <div class="icon-dev-central">
                    <img src="assets/images/icon_dev_durable/lien_social.png" alt="lien social et rencontre">
                </div>
                <div class="title_dev_central">
                    <div>
                        <article>Créer du lien social</article>
                    </div>
                </div>

                <div class="block_text_dev">
                    <p>Brisez la glace et rencontrez des gens proche de vous.
                        Faites vous de nouveau amis.
                    </p>
                </div>
            </div>

            <!--Fin : vignette 1er pillier du développement durable -->

            <!--Début : vignette 1er pillier du développement durable -->

            <div class="dev-central-block">
                <div class="icon-dev-central">
                    <img src="assets/images/icon_dev_durable/plan%C3%A8te.png" alt="protection de la planète">
                </div>
                <div class="title_dev_central d-flex justify-content-center">
                    <div>
                        <article>Protéger la planète</article>
                    </div>
                </div>

                <div class="block_text_dev">
                    <p>La location d'objets a pour but de diminuer la surproduction industrielle
                        et ainsi lutter contre la pollution.
                    </p>
                </div>
            </div>

            <!--Fin : vignette 1er pillier du développement durable -->

            <!--Début : vignette 1er pillier du développement durable -->

            <div class="dev-central-block">
                <div class="icon-dev-central">
                    <img src="assets/images/icon_dev_durable/pig_piggy_bank.png" alt="économie tirelire">
                </div>
                <div class="title_dev_central d-flex justify-content-center">
                    <div>
                        <article>Favoriser l'économie locale</article>
                    </div>
                </div>

                <div class="block_text_dev">
                    <p>Des services et des locations à portées de tous pour améliorer
                        son pouvoir d'achat.
                    </p>
                </div>
            </div>

            <!--Fin : vignette 1er pillier du développement durable -->

        </section>
    </div>
</section>

<!--Fin section développement durable -->

<!-- Début slider : les dernières annonces -->

<div class="parent1 container-fluid">
    <div class="title13">Nos dernières propositions</div>
    <div class="owl-carousel owl-theme">

        <?php
        $dab = new Daba(0,ORDER_DESC);
        $nb_ad = $dab->getNbad();
        ?>

        <?php for ($i = 0; $i < $nb_ad; $i++): ?>

        <?php 
            $dab->NextAd();
            if ( $dab->getAd()->availableStatus == 'AVAILABLE' &&  //seulement les annonces publiées
                 $dab->getAd()->retiredStatus == 'RETIRED_OFF' ):  // et non retirées ou en pause...
        ?>

            <div class="items1">
                <a href="login.php">
                    <div class="last-prop">

                        <div class="dateAndPrice_lastAd">
                            <div class="date_lastAd">
                                <div>
                                    <article>le <span><?php echo explode(" ", $dab->getAd()->creationDate)[0]; ?></span>
                                    </article>
                                </div>
                            </div>

                            <div class="price_lastAd<?php if ($dab->getAd()->needBidAttribute == 'NEED') echo "Need"; else echo "Bid"; ?>">
                                <div>
                                    <article><?php echo $dab->getAd()->pricing; ?>
                                        € <?php if ($dab->getAd()->leasingSaleAttribute == 'LEASING') echo "/ heure"; ?></article>
                                </div>
                            </div>
                        </div>

                        <div class="central_last_ad">
                            <div class="photo_last_ad">
                                <img src="<?php echo $dab->getUsePortrait(); ?>">
                            </div>
                            <div class="last-prop-cental-right">
                                <div class="last_name_last">
                                    <article><?php echo $dab->getUse()->firstName ?></article>
                                </div>
                                <div class="main-star-rating-lastAd">
                                    <div class="star-rating-lastAd">
                                        <img src="assets/images/star-rating.png" width="14px;">
                                    </div>
                                    <div class="note_lastAd">
                                        <article>4/5</article>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="title_last_ad">
                            <article>
                                <?php if ($dab->getAd()->needBidAttribute == 'NEED') echo "Je cherche";
                                else echo "Je loue";
                                echo " " . $dab->getObj()->label; ?>
                            </article>
                        </div>

                        <div class="city_last_ad">
                            <div class="icon-local-lastAd">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                            <div>
                                <article><?php echo $dab->getUse()->addressCity; ?></article>
                            </div>
                        </div>
                    </div>
                </a>
                </a>
            </div>

        <?php endif; ?>     
        <?php endfor; ?>

    </div>
</div>


<?php include('Partials/footer.php'); ?>

<!-- Owl Carousel jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<!-- Owl Carousel JavaScript -->
<script type="text/javascript" src="/assets/JavaScript/owl.carousel.min.js" async></script>
<!-- Optional JavaScript -->
<script type="text/javascript" src="/assets/JavaScript/app.js" async></script>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<!-- The jQuery script of the slider -->
<script type="text/javascript" src="/assets/JavaScript/slider_moments.js" async></script>

</body>

