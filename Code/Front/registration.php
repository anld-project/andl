<?php
$title = "Page d'inscription";
require_once 'functions.php';

if ($_POST && !empty($_POST)) {

    if (!empty($_POST['firstname'])
        &&!empty($_POST['mobilenumber'])
        &&!empty($_POST['email'])
        && !empty($_POST['password'])
        && !empty($_POST['password2']))
    {
        if ($_POST['password'] == $_POST['password2']) {
            registerUser($_POST['email'], $_POST['password'], $_POST['firstname'], $_POST['mobilenumber']);
        } else {
            redirect('login.php?error=field-empty');
        }
    } else {
        redirect('login.php?error=field-empty');
    }
}

?>
<?php include('Partials/head.php'); ?>


<body>

<?php include('Partials/menuBarNoConnected.php'); ?>

<main class="mainIns" id="mainPicture1">

    <div class="container d-flex justify-content-center" class="rounded">
        <div class="card" style="width:400px" >
            <article class="card-body mx-auto" style="max-width: 500px;">
                <h4 class="card-title pb-2 text-center">Inscription</h4>
                <form method="post">
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                        </div>
                        <input name="firstname" id="firstname" class="form-control" placeholder="Prénom" type="text">
                    </div> <!-- form-group// -->
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input name="email" id="email" class="form-control" placeholder="Adresse mail" type="email">
                    </div> <!-- form-group// -->
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                        </div>

                        <input name="mobilenumber" id="mobilenumber" class="form-control" placeholder="Numéro de téléphone" type="text">
                    </div> <!-- form-group// -->

                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="password" id="password" class="form-control" placeholder="Mot de passe"
                               type="password">
                    </div> <!-- form-group// -->
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="password2" id="password2" class="form-control" placeholder="Répéter mot de passe"
                               type="password">
                    </div> <!-- form-group// -->
                    <div class="form-group pt-2">
                        <a href="utilisateur_connu.html">
                            <button type="submit" class="btn btn-primary btn-block">S'inscrire</button>
                        </a>
                    </div> <!-- form-group// -->
                    <p id="haveAnAccount" class="text-center">Vous avez déja un compte? <a href="login.php">Connexion</a></p>
                </form>
            </article>
        </div>
    </div>
</main>
</body>

<?php include('Partials/scriptLinksBootstrap.php'); ?>