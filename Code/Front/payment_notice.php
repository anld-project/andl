<?php
$title = "Notification de paiement";
require_once('functions.php');
require_once('./assets/class/user.class.php');
include('partials/head.php');

$_SESSION['page'] = "payment_notice.php";

$idAdMate = 0;

if ($_GET) {
    if ($_GET['idAdMate']) {
        $idAdMate = $_GET['idAdMate'];
    }
}

$ad = GetAdId($idAdMate);
$adobj = GetIdObject(GetAdMoment($ad)->id_object);
$use = NextUser(GetUser($ad->idUser));

if ($ad->priceAttribute != 'Global') $totalPrice = priceEstimate($ad->pricing,$ad->priceAttribute,$ad->duration);
else $totalPrice = $ad->pricing;

?>


<body>

<?php include('Partials/menuBarConnected.php'); ?>
    
<div class="mainPaymentNotice">
    <div id="mainPaymentNoticeChild">
        <div class="titlePaymentNotice">
            <div>
                <article>A vous de conclure !</article>
            </div>
        </div>
        <div class="mainBlockPayNot">
            <section class="subTitlePayNot">
                <div>
                    <article>Vous venez de faire affaire sur</article>
                </div>
                <div>
                    <img src="assets/images/logos/logo-vopus-accroche.gif">
                </div>
            </section>
            <section id="mainCentralPayNot">
                <div id="detailsPayNot">
                    <div id="detailsPayNotChild">
                        <div id="titleDetailsPayNot">
                            <article><strong>Détails du paiement :</strong></article>
                        </div>
                        <div id="pricePayNot">
                            <article><strong><?php echo $totalPrice; ?></strong><strong>
                                    €</strong><span>  x</span><span>  <?php echo TAX; ?></span><span> %*</span><span> =</span><strong>
                                    <?php echo round(priceTax($totalPrice)); ?></strong><strong> €</strong></article>
                        </div>
                        <div id="infoComPayNot">
                            <p>* représente la commission dûe à la protection de la transaction</p>
                        </div>
                    </div>
                    <div id="title1paymentNot">
                            <div>
                                <h5>utilise</h5>
                            </div>
                            <div id="logo-payment-notice">
                                <img src="assets/images/logos/logo-vopus-accroche.gif">
                            </div>

                    </div>
                </div>
                <div id="thumbnailsPayNot">
                    <div class="photoAndInfoPayNot">
                        <div id="photoAndCityPayNot">
                            <div id="photoPayNot">
                                <img src="<?php echo GetUserPortrait($ad->idUser); ?>">
                            </div>
                            <div id="cityPayNot">
                                <div id="cityPayNotChild">
                                    <article>
                                        <i class="fas fa-map-marker-alt"></i>
                                        <?php echo $use->addressCity; ?>
                                    </article>
                                </div>
                            </div>
                        </div>
                        <div id="contactInfoPayNot">
                            <div>
                                <article>
                                    <strong><?php echo $use->firstName; ?></strong>
                                </article>
                            </div>
                            <div>
                                <article><?php echo $use->mobileNumber; ?></article>
                            </div>
                            <div>
                                <article><?php echo $use->email; ?></article>
                            </div>
                        </div>
                    </div>
                    <div class="summuaryPayNot">
                        <div class="summuaryPayNotChild">
                            <div>
                                <article>
                                    <strong><?php echo $adobj->label; ?></strong>
                                </article>
                            </div>
                            <div>
                                <article>

                                    
                                <?php if($ad->priceAttribute != 'Global'): ?>
                                    <span>Prix total </span><strong><?php echo $totalPrice; ?></strong><strong> €</strong><br>
                                <?php else: ?>
                                    <span> Prix global </span><strong><?php echo $totalPrice; ?></strong><strong> €</strong><br>
                                <?php endif; ?>
                                    

                                </article>
                            </div>
                            <div>
                                <article>Paiement Paypal</article>
                            </div>

                            <!-- Les 2 prochaines lignes (durée de location et caution) sont nécessaires uniquement lors d'une location.
                                  Lors d'un service seules les 3 premières sont suffisantes  -->

                            <div>
                                <article>Durée de location : <strong> <?php echo timeListConvert($ad->duration); ?></strong></article>
                            </div>
                            <div>
                                <article>Montant de la caution : <strong>30 </strong><strong>€</strong></article>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="greatBlockPayment">
                <div class="greatBlockPayChild1">
                    <div id="logoPayPalNot">
                        <img src="assets/images/PP_logo_h_150x38.png">
                    </div>
                </div>
                <div id="btnPayPayPal">
                    <div>
                        <!--a href="https://www.paypal.com/fr" target="_blank"-->
                        <a href="/my_deals.php?lastdealad=<?php echo $ad->id; ?>" target="_blank">
                            <button type="button" class="btn btn-info">Payer avec PayPal</button>
                        </a>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<?php include('Partials/scriptLinksBootstrap.php'); ?>
