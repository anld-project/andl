
window.onload = function () {


//function log($text){
//    $log.innerHTML += $text + '<br>';
//}

//const $log = document.querySelector('#log');
//$log.innerHTML = "==== LOG ====<br>";

const $img = document.querySelector('div .imagePreviewProfile');
const $inp = document.querySelector('#picinp');

    
$inp.addEventListener('click', (ev) => {
    ev.preventDefault();
        
    const data = new FormData();
    data.append('choix[]', $img.outerHTML);

/* ---> fetch : transmission div image au php pour sauvegarde dans file system */

    const promise = fetch("profile_edition_action.php", {
        method: 'POST',
        body: data,
    }).then((response) => {
        if (response.status !== 200) {
            throw new Error('Response not OK');
        }
         return response.json();
        })
        .then((obj) => {
            //log('FETCH ACTION OK');
            //log('OBJ = '+obj);
            window.location.assign("profile.php");
        })
        .catch(function(error) {
          alert('FETCH ERROR : ' + error.message);
        });    
    
/* <--- fetch */

});

/* ---> fetch : recuperation du div image s'il existe dans le file system */
     
    const buf = new FormData();
    buf.append('choix[]', '===TEST ALNED===');

    const promise = fetch("profile_edition_image.php", {
        method: 'POST',
        body: buf,
    }).then((response) => {
        if (response.status !== 200) {
            throw new Error('Response not OK');
        }
         return response.json();
        })
        .then((obj) => {
            //log('FETCH IMAGE OK');
            //log('OBJ = '+obj);
            
            if (obj != "") {

                const val = $img.children.length;
                for (let i = 0; i < val; i++) {
                    $img.children[0].remove();
                }
                $img.innerHTML = obj;
            }
              
        })
        .catch(function(error) {
          alert('FETCH ERROR : ' + error.message);
        });
    

/* <--- fetch */
    
}