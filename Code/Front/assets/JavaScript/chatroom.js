window.onload = function () {


    function log($text){
        $log.innerHTML += $text + '<br>';
    }


    const $log = document.querySelector('#log');
//$log.innerHTML = "==== LOG ====<br>";

    const $bads = document.querySelectorAll('[bad-id]');
    const $cros = document.querySelectorAll('.croix');

    $bads.forEach(function ($bad)
    {
        $bad.addEventListener('click', function () {
            window.location.assign("chatroom.php?chatroom=" + $bad.getAttribute('bad-id'));
        });
    });


    $cros.forEach(function ($cro)
    {
        $cro.addEventListener('click', function () {
            window.location.assign("chatroom.php?adelete=" + $cro.getAttribute('cro-id'));
        });
    });

};

