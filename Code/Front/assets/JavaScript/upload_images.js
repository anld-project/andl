$(".imgAdd").click(function(){
    $(this).closest(".row").find('.imgAdd')
        .before('<div class="imgUp2"><div class="imagePreview2"></div><label><input type="file" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fas fa-times"></i></div>');
});
$(document).on("click", "i.del" , function() {
    $(this).parent().remove();
});



$(function() {
    $(document).on("change",".uploadFile", function()
    {
        let uploadFile = $(this);
        let files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test( files[0].type)){ // only image file
            let reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function(){ // set image data as background of div
                //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
                uploadFile.closest(".imgUp2").find('.imagePreview2').css("background-image", "url("+this.result+")");
                uploadFile.closest(".imgPhotoProfile").find('.imagePreviewProfile').css("background-image", "url("+this.result+")");
            }
        }

    });
});


const $imgAdd = document.querySelector('div.imgAdd');
const $imgUp = document.querySelector('div.imgUp');
const $imagePreview = document.querySelector('div.imagePreview');

let nbClick=0;
let nbClickMax=2;

$imgAdd.addEventListener('click', function() {

    if ($imgUp.parentElement.children.length > 2) {
        alert('Veuillez effacer une photo');

        return;
    }

    /* Duplication et positionnement de la div class='imgUp' */
    let $imgUp2 = document.createElement('div');
    $imgUp2.className = 'imgUp2';
    $imgUp.insertAdjacentElement('afterend', $imgUp2);


    /* Duplication et positionnement de la div class='imagePreview' */
    let $imagePreview2 = document.createElement('div');
    $imagePreview2.className = 'imagePreview2';
    $imgUp2.insertAdjacentElement('beforeend', $imagePreview2);


    /* Création et positionnement d'un label' */
    let $label = document.createElement('label');
    $imagePreview2.insertAdjacentElement('beforeend', $label);


    /* Duplication et positionnement d'un input' */
    let $input = document.createElement('input');
    $input.className = 'uploadFile img';
    $input.type = 'file';
    // $input.value = 'Upload Photo';
    $label.insertAdjacentElement('beforeend', $input);


    /* Duplication et positionnement de la section class='iconUpload'  */
    let $section1 = document.createElement('section');
    $section1.className = 'iconUpload2';
    $label.insertAdjacentElement('beforeend', $section1);


    /* Duplication et positionnement de la div class='iconUploadChild' */
    let $div2 = document.createElement('div');
    $div2.className = 'iconUploadChild2';
    $section1.insertAdjacentElement('beforeend', $div2);


    /* Duplication et positionnement de <i> (photos) icon' */
    let $iconPhoto = document.createElement('i');
    $iconPhoto.className = 'far fa-images fa-3x';
    $div2.insertAdjacentElement('beforeend', $iconPhoto);
});

function count() {

    nbClick += 1;

    if(nbClick <= nbClickMax) {

       



    }else{
        alert('Veuillez effacer une image');
    }
}

//function showAlert()
//{
//    alert("Evènement de click détecté");
//}