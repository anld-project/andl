/* buttons: ripple effect */

const buttons = document.querySelectorAll('button');

buttons.forEach(button => {
    button.addEventListener('click', rippleEffect);
});

function rippleEffect(event) {
    let circle = document.createElement('div');
    this.appendChild(circle);
    circle.classList.add('ripple');
    circle.style.top = event.clientY - this.offsetTop + 'px';
    circle.style.left = event.clientX - this.offsetLeft + 'px';
    circle.style.transform = 'translate(-50%, -50%) scale(0)';
}






