<?php
    class Vopus {
        
        protected $req;
        protected $use;
        protected $ad;
        protected $mom;
        protected $obj;
        protected $she;
        
        public function getAd(){
            return($this->ad);
        }
        
        public function getMom(){
            return($this->mom);
        }
        
        public function getObj(){
            return($this->obj);
        }
        
        public function getUse(){
            return($this->use);
        }
        
        public function getShe(){
            return($this->she);
        }
        
        public function getUsePortrait(): string{
            return(GetUserPortrait($this->use->id));
        }
        
        public function getUsePic(): string{
            return("./assets/images/img_users/".$this->use->id);
        }
        
    }
?>
