<?php
    class Daba extends Vopus {
        
        protected $nb_ad;

        public function __construct(int $idUser, int $order){
            $this->nb_ad = CountAds($idUser);
            $this->req = GetAdOrder($idUser,$order);
        }
                
        public function NextAd(){
            
            $this->ad = NextAd($this->req);
            $idbo = GetAdMoment($this->ad);
            $this->mom = GetIdMoment($idbo->id_moment);
            $this->obj = GetIdObject($idbo->id_object);
            $this->use = GetAdUser($this->ad);
            $this->she = GetAdShedule($this->ad);
            
        }
        
        public function getDisplayStatus(string $available,string $retired): string{
            
            if ($retired != 'RETIRED_ON')
            {
                switch($available)
                {
                    case 'AVAILABLE':
                        $ret = "PUBLIEE";
                        break;
                    case 'NOT_AVAILABLE':
                        $ret = "RETIREE";
                        break;
                    case 'CURRENT':
                        $ret = "EN COURS";
                        break;
                    case 'FINISHED':
                        $ret = "FINIE";
                        break;
                    default:
                        $ret = "";
                }
            } else $ret = "RETIREE";
            
            return($ret);
        }
        
        public function getNbad(): int{
            return($this->nb_ad);
        }

        public function setStatusRetired(string $status){
            setAdStatusRetired($this->ad->id,$status);
        }
    }
?>
