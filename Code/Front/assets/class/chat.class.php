<?php
    class Chat extends Vopus {
        
        protected $idUser;
        protected $side;
        
        protected $chat;

        public function __construct(int $id){
            $this->idUser = $id;
            $this->req = GetChat($id);
        }
        
        public function NextChat(){
            
            $this->chat = NextChat($this->req);
            
            if ($this->idUser == $this->chat->idUserRight) $this->side = CHAT_RIGHT;
            else $this->side = CHAT_LEFT;
            
            return ($this->chat);
        }
        
        public function seekAd($idAd){
            
            $this->ad = GetAdId($idAd);

            $idbo = GetAdMoment($this->ad);
            $mom = GetIdMoment($idbo->id_moment);
            $this->obj = GetIdObject($idbo->id_object);
            $this->use = GetAdUser($this->ad);
            $this->she = GetAdShedule($this->ad);

            return($this->ad);
        }
        
        public function seekAdSideRight(){
            
            if ($this->side == CHAT_RIGHT) $idAd = $this->chat->idAdRight;
            else $idAd = $this->chat->idAdLeft;
            
            return($this->seekAd($idAd));
        }
        
        public function seekAdSideLeft(){
            
            if ($this->side == CHAT_LEFT) $idAd = $this->chat->idAdRight;
            else $idAd = $this->chat->idAdLeft;
            
            return($this->seekAd($idAd));
        }
        
        public function getDeal(): int{
            
            if ($this->side == CHAT_RIGHT) {$deal = $this->chat->dealRight;}
            else {$deal = $this->chat->dealLeft;}
            
            return($deal);
        }
        
        public function nextDeal(): int{
            
            if ($this->side == CHAT_RIGHT)
            { 
                $my_deal = $this->chat->dealRight;
                $mate_deal = $this->chat->dealLeft;
                $mate_side = CHAT_LEFT;
            } 
            else
            { 
                $my_deal = $this->chat->dealLeft;
                $mate_deal = $this->chat->dealRight;
                $mate_side = CHAT_RIGHT;
            }            
            
            $deal = $my_deal;
            
            if ( $my_deal == DEAL_NONE )
            {
                if ( $mate_deal == DEAL_WAIT || $mate_deal == DEAL_DONE )
                {
                    $deal = DEAL_DONE;
                    StoreDeal($this->chat->id,DEAL_DONE,$this->side);
                    StoreDeal($this->chat->id,DEAL_DONE,$mate_side);
                }
                else
                {
                    $deal = DEAL_WAIT;
                    StoreDeal($this->chat->id,DEAL_WAIT,$this->side);
                }
            }
            elseif ( $my_deal == DEAL_WAIT )
            {
                if ( $mate_deal == DEAL_WAIT || $mate_deal == DEAL_DONE )
                {
                    $deal = DEAL_DONE;
                    StoreDeal($this->chat->id,DEAL_DONE,$this->side);
                    StoreDeal($this->chat->id,DEAL_DONE,$mate_side);
                }
            }
            elseif ( $my_deal == DEAL_DONE )
            {   //reset des deals (annulation transaction)
                    $deal = DEAL_NONE;
                    StoreDeal($this->chat->id,DEAL_NONE,$this->side);
                    StoreDeal($this->chat->id,DEAL_NONE,$mate_side);
            }
 
            
            return($deal);
        }
        
        public function getPicDeal($deal): string{
                        
            $pic = "./assets/images/img_chatroom/deal_";
            if ($deal == DEAL_NONE) $pic .= "none.png";
            elseif ($deal == DEAL_WAIT) $pic .= "wait.png";
            else $pic .= "done.png";
            
            return($pic);
        }
        
        public function getTextDeal($deal): string{
                        
            if ($deal == DEAL_NONE)
            {
                $text = "Appuyer sur DEAL pour conclure...";
            }
            elseif ($deal == DEAL_WAIT)
            {
                $text = "DEAL en attente de reponse de<br>".$this->getUse()->firstName." ...";
            }
            else
            {
                $text = "félicitation : le DEAL est conclut !";
            }
            
            return($text);
        }
        
        
        public function getSide(): int{
            return($this->side);
        }
        
        public function getSideStr(): string{
            
            if ($this->side == CHAT_RIGHT) $ret = "right";
            else $ret = "left";
            
            return($ret);
        }
        
        public function __clone(){
            $this->idUser = $this->idUser;
            $this->side = $this->side;
            $this->chat = $this->chat;
            $this->req = $this->req;
            $this->ad = $this->ad;
            $this->obj = $this->obj;
            $this->use = $this->use;
            $this->she = $this->she;
        }
                
    }
?>
