-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 20, 2019 at 02:33 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anld`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `registrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL DEFAULT 'MALE',
  `mobileNumber` int(13) DEFAULT NULL,
  `addressNumber` varchar(255) DEFAULT NULL,
  `addressStreet` varchar(255) DEFAULT NULL,
  `addressPostalCode` int(5) DEFAULT NULL,
  `addressCity` varchar(255) DEFAULT NULL,
  `kmVisibility` int(4) DEFAULT '5',
  `verified` tinyint(1) DEFAULT '0',
  `banned` tinyint(1) DEFAULT '0',
  `dealCounter` int(4) DEFAULT '0',
  `userPictureLink` varchar(255) DEFAULT NULL,
  `paypalReferences` varchar(255) DEFAULT NULL,
  `presentation` varchar(255) DEFAULT NULL,
  `birthdayDate` varchar(255) DEFAULT NULL,
  `lastDealAd` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `registrationDate`, `firstName`, `lastName`, `email`, `password`, `gender`, `mobileNumber`, `addressNumber`, `addressStreet`, `addressPostalCode`, `addressCity`, `kmVisibility`, `verified`, `banned`, `dealCounter`, `userPictureLink`, `paypalReferences`, `presentation`, `birthdayDate`, `lastDealAd`) VALUES
(1, '2019-12-20 14:12:42', 'homer', 'simpson', 'homer@fox.usa', '$2y$10$lTcaoude2ulqFKFtAXsh.u30M21wBjMUrGnr32pvBnw5NgBs1z3GW', 'MALE', 1122334455, '1', 'rue de la serie', 998877, 'Springfield', 8, 1, 0, 3, 'user.png', NULL,'Celebre amateur de donuts et employé dans une centrale nucleaire, je ne m\'inquiete de rien.','1973-01-02',0),
(2, '2019-12-20 15:14:33', 'asterix', 'uderzo', 'asterix@village.fr', '$2y$10$IbfLv.FbJHVgDugG20oAM.qVoVuKcmfisrB3Chp.S3AxiAafwoC7O', 'MALE', 1231231234, '12', 'rue de la place', 10000, 'village gaulois', 24, 1, 0, 15, 'user.png', NULL,'je suis un courageux guerrier, qui se sert non seulement de la potion magique mais aussi de son intelligence pour déjouer les plans de Jules César et défendre son village de l\'envahisseur.','1965-08-27',4),
(3, '2019-12-20 15:16:09', 'tintin', 'herge', 'tintin@moulinsart.be', '$2y$10$FtOoQPoaozCl7Sy8WmX7nei3y22PisWWa7jHV.r8g4htzM1YZpVGS', 'MALE', 1020304050, '56', 'allée de moulinsart', 98765, 'bruxelles', 7, 1, 0, 17, 'user.png', NULL,'Je suis un jeune reporter d\'age indefinit, toujours accompagné dans mes voyages par mon fidele chien milou.','1978-12-07',0),
(4, '2019-12-20 15:17:39', 'luke', 'luky', 'luke@farwest.usa', '$2y$10$4gEqaHQO/3WawrMXMGeZZ.NSQyTJXyYc2y0XQyYbhVhNIFz2p6Ghi', 'MALE', 9988776655, '31', 'avenue de sans fransisco', 50120, 'new-york', 5, 9, 2, 21, 'user.png', NULL,'Je suis Lucky Luke et je me promene dans les contrées arides du far west. venez me retrouver au saloon.','1867-05-18',0),
(5, '2019-12-20 15:18:39', 'obelix', 'uderzo', 'obelix@village.fr', '$2y$10$IbfLv.FbJHVgDugG20oAM.qVoVuKcmfisrB3Chp.S3AxiAafwoC7O', 'MALE', 4321987621, '27', 'rue de la potion magique', 20000, 'village gaulois', 18, 1, 2, 6, 'user.png', NULL,'c\'est moi Obelix , fidele compagnon d\'asterix et grand mangeur de sangliers. je suis tombé dans la potion magique petit.','1975-08-21',0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
