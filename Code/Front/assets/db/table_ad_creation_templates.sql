-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 22, 2019 at 08:58 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anld`
--

-- --------------------------------------------------------

--
-- Table structure for table `ad`
--

CREATE TABLE `ad` (
  `id` int(10) UNSIGNED NOT NULL,
  `idShedule` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `needBidAttribute` enum('NEED','BID') NOT NULL DEFAULT 'NEED',
  `leasingSaleAttribute` enum('LEASING','SALE','FREE') NOT NULL DEFAULT 'LEASING',
  `momentObjectAttribute` enum('MOMENT','OBJECT') NOT NULL DEFAULT 'MOMENT',
  `availableStatus` enum('NOT_AVAILABLE','AVAILABLE','CURRENT','FINISHED') NOT NULL DEFAULT 'NOT_AVAILABLE',
  `retiredStatus` enum('RETIRED_OFF','RETIRED_PAUSE','RETIRED_ON') NOT NULL DEFAULT 'RETIRED_OFF',
  `introductionText` varchar(255) DEFAULT NULL,
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pricing` int(4) DEFAULT '0',
  `priceAttribute` enum('Heure','Jour','Global') NOT NULL DEFAULT 'HEURE',
  `duration` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ad`
--

INSERT INTO `ad` (`id`, `idShedule`, `idUser`, `needBidAttribute`, `leasingSaleAttribute`, `momentObjectAttribute`, `availableStatus`, `retiredStatus`, `introductionText`, `creationDate`, `pricing`, `priceAttribute`, `duration`) VALUES
(1, 1, 3, 'NEED', 'LEASING', 'OBJECT', 'AVAILABLE', 'RETIRED_OFF', 'Je suis actuellement immobilisé chez moi pour 2 mois, suite a un accident de ski.\r\nJe cherche une aide pour faire mes courses et me les livrer a domicile.\r\ndeux fois par semaine , ca serait bien...\r\n', '2019-12-22 09:36:28', 25, 'Heure', 3),
(2, 2, 2, 'NEED', 'LEASING', 'OBJECT', 'AVAILABLE', 'RETIRED_OFF', 'je dois repeindre le plafond de mon salon. plutot qu\'acheter un escabeau je\r\nprefere louer. je pense terminer en une semaine mon plafond.\r\n', '2020-03-22 10:18:00', 30, 'Jour', 6),
(3, 3, 1, 'NEED', 'LEASING', 'OBJECT', 'AVAILABLE', 'RETIRED_OFF', 'Qui me loue son ampli cinema ? j\invite mon patron mrBarns a la maison et je veux l\'impressionner en lui montrant les derniers episodes des Simpsons sur mon ecran du salon avec du gros son !\r\n', '2019-12-22 09:18:17', 40, 'Heure', 8),
(4, 0, 4, 'BID', 'SALE', 'OBJECT', 'AVAILABLE', 'RETIRED_OFF', 'je vends ma tondeuse car j\'habite a present en ville dans un appartement. fini\r\nla corvée de pelouse ! la tondeuse est en bon etat et je me deplace pour l\'apporter.\r\n', '2019-11-12 23:10:51', 150, 'Global', 11),
(5, 2, 2, 'BID', 'LEASING', 'OBJECT', 'CURRENT', 'RETIRED_OFF', 'Je viens d\'acheter une television toute équipée alors je n\'ai plus besoin de mon ancien equippement audio.\n', '2019-12-25 18:02:00', 680, 'Global', 3),
(6, 1, 3, 'BID', 'SALE', 'OBJECT', 'AVAILABLE', 'RETIRED_OFF', 'Je suis actuellement immobilisé chez moi pour 2 mois, suite a un accident d\'escabeau !.\r\nDégouté de la vie, je veux me débarrasser de ce maudit objet...\r\n', '2020-02-05 22:45:03', 48, 'Global', 6),
(7, 3, 5, 'NEED', 'LEASING', 'OBJECT', 'AVAILABLE', 'RETIRED_OFF', 'Afin d\'ammeliorer la productivite de ma carriere, je souhaite a présent tailler mes menhirs avec un outil plus adapté a mes grosses mains.\n', '2007-04-02 12:25:38', 50, 'Heure', 21),
(8, 3, 2, 'BID', 'LEASING', 'OBJECT', 'AVAILABLE', 'RETIRED_OFF', 'Je suis trop petit pour me servir d\'un outil de chantier. Alors je vais le louer pour quelques sesterces.\n', '2012-11-23 18:01:59', 63, 'Heure', 18),
(9, 2, 2, 'NEED', 'LEASING', 'OBJECT', 'AVAILABLE', 'RETIRED_OFF', 'J\'aime me détendre avec Obelix et Panoramix autour d\'un bon café, après la bataille.\r\n', '2018-03-22 10:18:00', 23, 'Jour', 6),
(10, 2, 2, 'BID', 'LEASING', 'OBJECT', 'AVAILABLE', 'RETIRED_OFF', 'J\ai acheté un déguisement pour la fete du village mais il est trop grand pour moi, alors je m\'en débarrasse.\n', '2015-01-10 10:18:00', 36, 'Jour', 6),
(11, 2, 2, 'NEED', 'LEASING', 'OBJECT', 'FINISHED', 'RETIRED_OFF', 'mon PC est en panne\n', '2017-03-22 10:18:00', 36, 'Jour', 6)
;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ad`
--
ALTER TABLE `ad`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ad`
--
ALTER TABLE `ad`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
