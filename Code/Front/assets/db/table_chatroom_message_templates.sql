--
-- Table structure for table `chatroom`
--

CREATE TABLE `chatroom` (
  `id` int(10) UNSIGNED NOT NULL,
  `idUserRight` int(10) UNSIGNED NOT NULL,
  `idUserLeft` int(10) UNSIGNED NOT NULL,
  `idAdRight` int(10) UNSIGNED NOT NULL,
  `idAdLeft` int(10) UNSIGNED NOT NULL,
  `dealRight` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `dealLeft` int(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexes for table
--
ALTER TABLE `chatroom`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table
--
ALTER TABLE `chatroom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Dumping data for table
--

INSERT INTO `chatroom` (`id`, `idUserRight`, `idUserLeft`, `idAdRight`, `idAdLeft`, `dealRight`, `dealLeft`) VALUES
(1, 2, 3, 2, 6, 0, 0),
(2, 2, 1, 5, 3, 0, 0),
(3, 2, 5, 8, 7, 0, 0);




--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(10) UNSIGNED NOT NULL,
  `idChatroom` int(10) UNSIGNED NOT NULL,
  `idUser` int(10) UNSIGNED NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexes for table 
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table 
--
ALTER TABLE `message`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Dumping data for table
--

INSERT INTO `message` (`id`, `idChatroom`, `idUser`, `date` , `text`) VALUES
(1, 1, 2, '2019-02-11 14:00:00' , 'salut tintin ! comment va milou ?'),
(2, 1, 2, '2019-02-11 14:02:00' , 'idefix est mort - ecrasé sous un menhir. obelix fait la gueule'),
(3, 1, 3, '2019-02-11 14:01:00' , 'salut asterix. milou a mordu le capitaine haddock. comment va idefix ?'),
(4, 1, 3, '2019-02-11 14:03:00' , 'oui cela est bien triste. et a part ca tu me le prends mon escabeau ?'),
(5, 2, 2, '2019-02-01 14:27:00' , 'j\'ai une solution pour le son pourri de ta télé !'),
(6, 2, 2, '2019-02-02 15:18:00' , 'en plus, toute la famille simpson va en profiter...'),
(7, 2, 1, '2019-02-02 16:46:00' , 'salut asterix. occupe toi plutot des romains qui sont en train d\'envahir ton village.'),
(8, 3, 5, '2020-01-05 14:03:00' , 'asterix ! sors les mains de tes poches et viens m\'aider a la carriere de menhirs'),
(9, 3, 2, '2020-01-07 14:27:00' , 'Quoi ? tu ne connais pas ma trouvaille sur le chantier romain ?'),
(10, 3, 5, '2020-01-07 15:18:00' , 'ben non... explique.'),
(11, 3, 2, '2020-01-10 08:46:00' , 'c\est un marteau piqueur. idéal pour exprimer tout ton talent de casseur de pierres')
;




