<?php
require_once('./assets/class/vopus.class.php');
session_start();

define('OK',0);
define('ERROR',1);

//attribut objet
define('ATR_NONE',0);
define('ATR_OBJECT',1);
define('ATR_SERVICE',2);

//attribut moment
define('MOMENT_REFERENT',1);
define('MOMENT_USER',2);

//attribut annonce
define('LEASING',0);
define('SALE',1);
define('FREE',2);

define('ORDER_NONE',0);
define('ORDER_ASC',1);
define('ORDER_DESC',2);

define('TAX',8.1);

//chatroom
define('TWEET_BOTTOM_LEFT',0);
define('TWEET_BOTTOM_RIGHT',1);
define('PAGE_USER_RIGHT',0);
define('PAGE_USER_LEFT',1);

define('CHAT_RIGHT',1);
define('CHAT_LEFT',2);

define('ONGLET_NEED',1);
define('ONGLET_BID',2);

define('DEAL_NONE',0);
define('DEAL_WAIT',1);
define('DEAL_DONE',2);

//debug
define('ID_USER_HOMER',1);
define('ID_USER_ASTERIX',2);
define('ID_USER_TINTIN',3);
define('ID_USER_LUKE',4);
define('NB_PAGE_USER',1);  /* 1 standard (une page) , 2 en demo (double pages) */


function isLoggedIn(): bool
{
   
    if (isset($_SESSION['loggedIn'])) {
        return true;
    } 
    return false;
}


//Redirect à la page login quand l'utilisateur n'est pas connecté
function redirectToLogin(): void
{
   
    if(strpos($_SERVER['REQUEST_URI'], 'login.php') != 1) {
        if(!isLoggedIn()) {
            redirect('login.php');
        }
    }
}



function redirect(string $path): void
{
    header('Location: ' . $path);
}


function dump($var) {
    echo '<pre>' . var_export($var, true) . '</pre><br>';
}

function varinv($x): int {
    if ($x == 1) return 0;
    else return 1;
}

/**
 * Import SQL File
 *
 * @param $pdo
 * @param $sqlFile
 * @param null $tablePrefix
 * @param null $InFilePath
 * @return bool
 */

function importSqlFile($pdo, $sqlFile, $tablePrefix = null, $InFilePath = null)
{
	try {
		
		// Enable LOAD LOCAL INFILE
		$pdo->setAttribute(\PDO::MYSQL_ATTR_LOCAL_INFILE, true);
		
		$errorDetect = false;
		
		// Temporary variable, used to store current query
		$tmpLine = '';
		
		// Read in entire file
		$lines = file($sqlFile);
		
		// Loop through each line
		foreach ($lines as $line) {
			// Skip it if it's a comment
			if (substr($line, 0, 2) == '--' || trim($line) == '') {
				continue;
			}
			
			// Read & replace prefix
			$line = str_replace(['<<prefix>>', '<<InFilePath>>'], [$tablePrefix, $InFilePath], $line);
			
			// Add this line to the current segment
			$tmpLine .= $line;
			
			// If it has a semicolon at the end, it's the end of the query
			if (substr(trim($line), -1, 1) == ';') {
				try {
					// Perform the Query
					$pdo->exec($tmpLine);
				} catch (\PDOException $e) {
					echo "<br><pre>Error performing Query: '<strong>" . $tmpLine . "</strong>': " . $e->getMessage() . "</pre>\n";
					$errorDetect = true;
				}
				
				// Reset temp variable to empty
				$tmpLine = '';
			}
		}
		
		// Check if error is detected
		if ($errorDetect) {
			return false;
		}
		
	} catch (\Exception $e) {
		echo "<br><pre>Exception => " . $e->getMessage() . "</pre>\n";
		return false;
	}
	
	return true;
}


/**
 * Init database
 *
 */

//creation database demo (avec templates)

function initDb() {
    
    //recherche database anld
    
    $servname = "localhost"; $user = "root"; $pass = "root";
    
    try {
        $db = new PDO('mysql:host=localhost;dbname=anld;charset=utf8', $user, $pass);
    } 
    catch (Exception $e) {
        
        //database anld pas trouvée... il faut la creer
            
        try{
            $db = new PDO("mysql:host=localhost", $user, $pass);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $db->exec("CREATE DATABASE anld");
            $db->exec("use anld");
                        
            //rajout des tables moments/objets referents
            
            $res = importSqlFile($db, 'assets/db/db_init.sql');
        
            if ($res === false) {
                die('ERROR');
            }
            
            //rajout des templates user , annonce (pour demo)          
            
            $res = importSqlFile($db, 'assets/db/table_users_creation_templates.sql');
            if ($res === false) die('ERROR');
            $res = importSqlFile($db, 'assets/db/table_ad_creation_templates.sql');
            if ($res === false) die('ERROR');
            $res = importSqlFile($db, 'assets/db/table_shedule_creation_templates.sql');
            if ($res === false) die('ERROR');
            $res = importSqlFile($db, 'assets/db/table_date_creation_templates.sql');
            if ($res === false) die('ERROR');
            $res = importSqlFile($db, 'assets/db/table_mobox_templates.sql');
            if ($res === false) die('ERROR');
            $res = importSqlFile($db, 'assets/db/table_moment_templates.sql');
            if ($res === false) die('ERROR');
            
            
            $res = importSqlFile($db, 'assets/db/table_chatroom_message_templates.sql');
            if ($res === false) die('ERROR');
            
       }

        catch(PDOException $e){
            echo "*** Erreur CREATE DB *** " . $e->getMessage()."<br>";
        }
        
    }
    
    return $db;
}

//creation database vide (sans templates demo)

function initDbVoid() {
    
    //recherche database anld
    
    $servname = "localhost"; $user = "root"; $pass = "root";
    
    try {
        $db = new PDO('mysql:host=localhost;dbname=anld;charset=utf8', $user, $pass);
    } 
    catch (Exception $e) {
        
        //database anld pas trouvée... il faut la creer
            
        try{
            $db = new PDO("mysql:host=localhost", $user, $pass);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $db->exec("CREATE DATABASE anld");
            $db->exec("use anld");
                        
            //creation des tables et rajout des tables moments/objets referents
            
            $res = importSqlFile($db, 'assets/db/db_init_void.sql');
        
            if ($res === false) {
                die('ERROR');
            }
        }
        catch(PDOException $e){
            echo "*** Erreur CREATE DB *** " . $e->getMessage()."<br>";
        }
        
    }
    
    return $db;
}


/**
 * gestion connection/enregistrement utilisateur
 *
 */

function connectUser(string $email, string $password)
{
    $db = initDb();

    $request = $db->prepare("SELECT * FROM `users` WHERE email = ?");

    $request->execute([$email]);
    
    $user = $request->fetch(PDO::FETCH_OBJ);

    //echo "USER SESSION ID = (".$user->id.")<br>";
    
    dump($user);
    
    if ($user == false) {
        redirect('login.php?auth=fail');
    }
    
    if(password_verify($password, $user->password)) {
        $_SESSION['loggedIn'] = $email;
        $_SESSION['idUser'] = $user->id;
        redirect('index_user.php');
    } else {
        redirect('/login.php?auth=fail');
    }
    
}

function registerUser(string $email, string $password, string $firstName, string $mobileNumber)
{
    $db = initDb();

    $request = $db->prepare("SELECT * FROM `users` WHERE email = ?");
    $request->execute([$email]);
    $user = $request->fetch(PDO::FETCH_OBJ);

    if ($user == true) {
        redirect('/login.php?auth=fail'); //deja inscrit -> erreur
    }
    else
    {   //inscription de l'utilisateur dans la base de registre
        
        $request = $db->prepare(" INSERT INTO users(email,password,firstName,mobileNumber)
                                    VALUES (:email,:password,:firstName,:mobileNumber)");

        $phash = password_hash($password, PASSWORD_DEFAULT);
        
        $request->execute(array(
                                ':email' => $email,
                                ':password' => $phash,
                                ':firstName' => $firstName,
                                ':mobileNumber' => $mobileNumber
                                ));
        
        redirect('login.php');
        
    }
}




//Database : gestion UTILISATEUR

/* recherche infos utilisateur */

function searchUser(string $email){
    
    $db = initDb();

    $request = $db->prepare("SELECT * FROM `users` WHERE email = ?");
    $request->execute([$email]);    
    $user = $request->fetch(PDO::FETCH_OBJ);
    
    return($user);
}


/* Creation et enregistrement de toutes les données utilisateur */

function registerUserAll(
                            string $email,
                            string $password,
                            string $firstName,
                            string $mobileNumber,
                            string $lastName,
                            string $addressNumber,
                            string $addressStreet,
                            string $addressPostalCode,
                            string $addressCity,
                            string $kmVisibility,
                            string $verified,
                            string $banned,
                            string $dealCounter,
                            string $userPictureLink
)
{
    $db = initDb();

    $request = $db->prepare("SELECT * FROM `users` WHERE email = ?");

    $request->execute([$email]);
    
    $user = $request->fetch(PDO::FETCH_OBJ);

    if ($user == true) {
        redirect('/login.php?auth=fail'); //deja inscrit -> erreur
    }
    else
    {   //inscription de l'utilisateur dans la base de registre

        $request = $db->prepare(" INSERT INTO users(email,password,firstName,mobileNumber,lastName,addressNumber,addressStreet,addressPostalCode,addressCity,kmVisibility,verified,banned,dealCounter,userPictureLink) 
        VALUES (:email,:password,:firstName,:mobileNumber,:lastName,:addressNumber,:addressStreet,:addressPostalCode,:addressCity,:kmVisibility,:verified,:banned,:dealCounter,:userPictureLink)");

        $phash = password_hash($password, PASSWORD_DEFAULT);
        
        $request->execute(array(
                                ':email' => $email,
                                ':password' => $phash,
                                ':firstName' => $firstName,
                                ':mobileNumber' => $mobileNumber,
                                ':lastName' => $lastName,
                                ':addressNumber' => $addressNumber,
                                ':addressStreet' => $addressStreet,
                                ':addressPostalCode' => $addressPostalCode,
                                ':addressCity' => $addressCity,
                                ':kmVisibility' => $kmVisibility,
                                ':verified' => $verified,
                                ':banned' => $banned,
                                ':dealCounter' => $dealCounter,
                                ':userPictureLink' => $userPictureLink
                                ));
        
        redirect('login.php');
    }
        
}

/* Enregistrement de toutes les données utilisateur courant */


function storeUser(
                            int    $idUser,
                            string $email,
                            string $password,
                            string $firstName,
                            string $mobileNumber,
                            string $lastName,
                            string $addressNumber,
                            string $addressStreet,
                            string $addressPostalCode,
                            string $addressCity,
                            string $kmVisibility,
                            string $verified,
                            string $banned,
                            string $dealCounter,
                            string $userPictureLink,
                            string $gender,
                            string $presentation,
                            string $birthdayDate
)
{
/*
    echo "STORE USER(".$idUser.") email(".$email.") password(".$password.") firstName(".$firstName.") mobileNumber(".$mobileNumber.") lastName(".$lastName.") addressNumber(".$addressNumber.")
    addressStreet(".$addressStreet.") addressPostalCode(".$addressPostalCode.") addressCity(".$addressCity.") kmVisibility(".$kmVisibility.") verified(".$verified.") banned(".$banned.")
    dealCounter(".$dealCounter.") userPictureLink(".$userPictureLink.") gender(".$gender.") presentation(".$presentation.") birthdayDate(".$birthdayDate.")<br>";
*/   
    $db = initDb();
  
    $request = $db->prepare("UPDATE users SET email = ?, password = ?, firstName = ?, mobileNumber = ?, lastName = ?, addressNumber = ?, addressStreet = ?, addressPostalCode = ?, addressCity = ?, kmVisibility = ?, verified = ?, banned = ?, dealCounter = ?, userPictureLink = ?, gender = ?, presentation = ?, birthdayDate = ? WHERE id = ?");
    
    $request->execute([
                            $email,
                            $password,
                            $firstName,
                            $mobileNumber,
                            $lastName,
                            $addressNumber,
                            $addressStreet,
                            $addressPostalCode,
                            $addressCity,
                            $kmVisibility,
                            $verified,
                            $banned,
                            $dealCounter,
                            $userPictureLink,
                            $gender,
                            $presentation,
                            $birthdayDate,
                            $idUser
                        ]);
}


function updateLastUserDeal(
                            int    $idUser,
                            int    $lastDealAdId
                            )
{
    $db = initDb();
  
    $request = $db->prepare("UPDATE users SET lastDealAd = ? WHERE id = ?");
    
    $request->execute([
                            $lastDealAdId,
                            $idUser
                      ]);
}


/* suppression d'un utilisateur */

function deleteUser($idUser){
    
    $db = initDb();

    $request = $db->prepare("DELETE FROM `users` WHERE id = ?");
    $request->execute([$idUser]);
    //$count = $request->rowCount();
    //echo "Effacement de " .$count. " users.<br>";   
   
    $request = $db->prepare("DELETE FROM `ad` WHERE idUser = ?");
    $request->execute([$idUser]);
    //$count = $request->rowCount();
    //echo "Effacement de " .$count. " ads.<br>";   
   
    $request = $db->prepare("DELETE FROM `chatroom` WHERE idUserRight = ? OR idUserLeft = ?");
    $request->execute([$idUser,$idUser]);
    //$count = $request->rowCount();
    //echo "Effacement de " .$count. " chatrooms.<br>";   
   
    $request = $db->prepare("DELETE FROM `message` WHERE idUser = ?");
    $request->execute([$idUser]);
    //$count = $request->rowCount();
    //echo "Effacement de " .$count. " messages.<br>";   
   
    //a faire... effacement des dependances (mobox) des ads supprimées 
    //et aussi les pics de l'utilisateur
    
    unset($_SESSION['loggedIn']);
    
    
}


/* importation photo utilisateur */

function uploadPic($idUser,$fileName){
    
    $target_dir = "./assets/images/img_users/";
    mkdir($target_dir.$idUser, 0777);

    $target_file = $target_dir.$idUser."/".$fileName.".png";
    echo "TARGET FILE =".$target_file."<br>";

    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error

    $ret = ERROR;

    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            $ret = OK;
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }

 return($ret);
}



function GetUser($id_user): PDOStatement
{
    $db = initDb();
    
    $request = $db->prepare("SELECT * FROM `users` WHERE id = ?");
    $request->execute([$id_user]);
    
    return($request);
}
   
function NextUser($request)
{
    $use = $request->fetch(PDO::FETCH_OBJ);
    return($use);
}

function GetUserMap($name): string {
    
    if ($name == 'asterix') $map = "assets/images/village_gaulois.png";
    elseif ($name == 'homer') $map = "assets/images/simpson_ville.png";
    elseif ($name == 'tintin') $map = "assets/images/tintin_moulinsart.png";
    elseif ($name == 'luke') $map = "assets/images/luke_saloon.png";
    elseif ($name == 'obelix') $map = "assets/images/village_gaulois.png";
    else $map = "assets/images/map-paris4.png";
    
    return($map);
} 

function GetUserPortrait($user_id): string{
    
            $path = "./assets/images/img_users/".$user_id."/user.";
            
            if(file_exists($path."jpeg")){
                $type = "jpeg";
            }
            else if (file_exists($path."jpg")){
                $type = "jpg";
            } else $type = "png";
    
            return($path.$type);    
}


//Database : Gestion des MOMENTS

function CountMoments(): int
{
    $db = initDb();
    
    $request = $db->prepare("SELECT COUNT(*) FROM `moment`");
    $request->execute();
    $resultat = $request->fetch(PDO::FETCH_ASSOC);
    
    return($resultat['COUNT(*)']);
}


function GetMoments(): PDOStatement
{
    $db = initDb();
    
    $request = $db->prepare("SELECT * FROM `moment`");
    $request->execute();
    
    return($request);
}

function getMomentId(int $id_mom)
{
    $db = initDb();
    
    $request = $db->prepare("SELECT * FROM `moment` WHERE id=?");
    $request->execute([$id_mom]);
    $moment = $request->fetch(PDO::FETCH_OBJ);
    return($moment);
}
    
function getMomentBanner(int $id_mom)
{
    $tab = [
        'anniversaire',
        'naissance',
        'bricolage_exterieur',
        'bricolage_intérieur',
        'demenagement',
        'reparation_voiture',
        'fete_evenement',
        'installation_ville',
        'jardinage',
        'mariage',
        'menage',
        'partage_activités'
    ];
    return($tab[$id_mom-1].".png");
}
    
function GetObjectsAd($id_ad)
{
    $db = initDb();
    
    $request = $db->prepare("SELECT id_object FROM `mobox` WHERE id_ad = ?");
    $request->execute([$id_ad]);
    
    $idob = $request->fetchAll(PDO::FETCH_OBJ);
    return($idob);
}
  
function NextMoment($request)
{
    $moment = $request->fetch(PDO::FETCH_OBJ);
    return($moment);
}




//Database : Gestion des ANNONCES

function storeAd(
                            int $idShedule,
                            int $idUser,
                            string $needBidAttribute,
                            string $leasingSaleAttribute,
                            string $momentObjectAttribute,
                            string $availableStatus,
                            string $introductionText,
                            int $pricing,
                            string $priceAttribute,
                            int $duration
): int
{
    $db = initDb();

    $req = $db->prepare("INSERT INTO ad(idShedule, idUser, needBidAttribute, leasingSaleAttribute, momentObjectAttribute, availableStatus, introductionText, pricing, priceAttribute, duration) VALUES (:idShedule,:idUser,:needBidAttribute,:leasingSaleAttribute,:momentObjectAttribute,:availableStatus,:introductionText,:pricing,:priceAttribute,:duration)");

    $req->execute(array(
                            ':idShedule' => $idShedule,
                            ':idUser' => $idUser,
                            ':needBidAttribute' => $needBidAttribute,
                            ':leasingSaleAttribute' => $leasingSaleAttribute,
                            ':momentObjectAttribute' => $momentObjectAttribute,
                            ':availableStatus' => $availableStatus,
                            ':introductionText' => $introductionText,
                            ':pricing' => $pricing,
                            ':priceAttribute' => $priceAttribute,
                            ':duration' => $duration
                            ));
    
    $req = $db->prepare("SELECT id FROM ad WHERE id=LAST_INSERT_ID()");    
    $req->execute();
    $obj = $req->fetch(PDO::FETCH_OBJ);
    
    return($obj->id);
}


function CountAds(int $idUser): int
{
    $db = initDb();
    
    $com = "SELECT COUNT(*) FROM `ad`";
    if ($idUser != 0) $com .= " WHERE idUser = ?";
    
    $request = $db->prepare($com);
    if ($idUser != 0) $request->execute([$idUser]); else $request->execute();
    $resultat = $request->fetch(PDO::FETCH_ASSOC);
    
    return($resultat['COUNT(*)']);
}


function GetAd(): PDOStatement
{
    $db = initDb();
    
    $request = $db->prepare("SELECT * FROM `ad`");
    $request->execute();
    
    return($request);
}
   

function GetAdOrder(int $idUser, int $order): PDOStatement
{
    $db = initDb();
    
    $com = "SELECT * FROM `ad`";
    if ($idUser != 0) $com .= " WHERE idUser = ?";
    if ($order == ORDER_DESC) $com .= " ORDER BY creationDate DESC";
    else if ($order == ORDER_ASC) $com .= " ORDER BY creationDate ASC";

    $req = $db->prepare($com);
    if ($idUser != 0) $req->execute([$idUser]); else $req->execute();
    
    return($req);
}

function GetAdId(int $id_ad)
{
    $db = initDb();
    
    $request = $db->prepare("SELECT * FROM `ad` WHERE id = ?");
    $request->execute([$id_ad]);
    $ad = $request->fetch(PDO::FETCH_OBJ);
    return($ad);
}
    
function setAdStatusRetired(int $id_ad,string $status)
{
    $db = initDb();
     
    $req = $db->prepare("UPDATE ad SET retiredStatus = ? WHERE id = ?");
    $req->execute([$status,$id_ad]);
}

function clearAdRetired()
{
    $db = initDb();
     
    $req = $db->prepare("DELETE FROM ad WHERE retiredStatus='RETIRED_ON' OR availableStatus='NOT_AVAILABLE'");
    $req->execute();
}

function NextAd($request)
{
    $ad = $request->fetch(PDO::FETCH_OBJ);
    return($ad);
}

function GetAdMoment($ad)
{
    $db = initDb();
    
    $request = $db->prepare("SELECT id_moment,id_object FROM `mobox` WHERE id_ad = ?");
    $request->execute([$ad->id]);
    
    $idmo = $request->fetchAll(PDO::FETCH_OBJ);
    return($idmo[0]);
}

function GetAdUser($ad)
{
    $db = initDb();
    
    $request = $db->prepare("SELECT id,firstName,lastName,userPictureLink,addressCity FROM `users` WHERE id = ?");
    $request->execute([$ad->idUser]);
    
    $user = $request->fetchAll(PDO::FETCH_OBJ);
    return($user[0]);
}

function GetAdShedule($ad)
{
    $db = initDb();
    
    $request = $db->prepare("SELECT id,startDate,endDate FROM `shedule` WHERE id = ?");
    $request->execute([$ad->idUser]);
    
    $shed = $request->fetchAll(PDO::FETCH_OBJ);
    return($shed[0]);
}

function GetIdMoment($idmo)
{
    $db = initDb();
    
    $request = $db->prepare("SELECT title FROM `moment` WHERE id = ?");
    $request->execute([$idmo]);
    
    $mom = $request->fetchAll(PDO::FETCH_OBJ);
    return($mom[0]);
}

function GetIdObject($idob)
{
    $db = initDb();
    
    $request = $db->prepare("SELECT * FROM `object` WHERE id = ?");
    $request->execute([$idob]);
    
    $obj = $request->fetchAll(PDO::FETCH_OBJ);
    return($obj[0]);
}


function budgetPriceConvert($index): int
{
    $euros = [5,10,15,20,25,30,35,40,45,50,60,70,80,90,100,120,150,200];
    return($euros[$index]);
}

function timeListConvert($index): string
{
    $timeInterval = ['inconnu','1 heure','2 heures','3 heures','1 demi-journée','1 journée',
             '2 jours','3 jours','4 jours','5 jours','6 jours','1 semaine','2 semaines',
             '3 semaines','1 mois','2 mois'];
    return($timeInterval[$index]);
}
 
function priceEstimate($budget,$priceAtr,$interval): float
{   //calcul nb heures par rapport a timeInterval
    //(1 jour compte pour 8 heures, 1 semaine pour 7 jours, 1 mois pour 4 semaines)
    $nbHour = [0,1,2,3,4,8,
             16,24,32,40,48,56,112,
             168,224,448];

    if ($priceAtr == 'Heure') { $x = $nbHour[$interval]; }
    elseif ($priceAtr == 'Jour') { $x = $nbHour[$interval]/8; }
    else { $x = 1; }
    
    return ( budgetPriceConvert($budget) * $x);
}

function priceTax($price){
    return($price + ($price * TAX)/100);
}

//Database : gestion du CALENDRIER avec ses DATES

function CountDate($she): int
{
    $db = initDb();
    
    $request = $db->prepare("SELECT COUNT(*) FROM `date` WHERE id_shedule = ?");
    $request->execute([$she->id]);
    $resultat = $request->fetch(PDO::FETCH_ASSOC);
    
    return($resultat['COUNT(*)']);
}

function GetSheduleDate($she): PDOStatement
{
    $db = initDb();
    
    $request = $db->prepare("SELECT * FROM `date` WHERE id_shedule = ?");
    $request->execute([$she->id]);
    
    return($request);
}

function NextDate($request)
{
    $date = $request->fetch(PDO::FETCH_OBJ);
    return($date);
}



//Database : Gestion des OBJETS


function GetObjects($mom)
{
    $db = initDb();
    
    $request = $db->prepare("SELECT id_object FROM `mobox` WHERE id_moment = ?");
    $request->execute([$mom->id]);
    
    $idob = $request->fetchAll(PDO::FETCH_OBJ);
    return($idob);
}
    
function NextObject($idob,$index,$atr)
{
    $db = initDb();

    if ($atr != 0) $sel = "SELECT id,label,attribut FROM `object` WHERE id = ? AND attribut = ?";
    else $sel = "SELECT id,label,attribut FROM `object` WHERE id = ?";
    
    $request = $db->prepare($sel);
    
    if ($atr != 0) $request->execute( array($idob[$index]->id_object, $atr) );
    else $request->execute( array($idob[$index]->id_object) );
    
    $obj = $request->fetch(PDO::FETCH_OBJ);
    return($obj);
}

function SearchObject($label)
{
    $db = initDb();
    
    $x = "SELECT id,label FROM object WHERE label LIKE '%".$label."%'";
    
    $request = $db->prepare($x);
    $request->execute();
    
    $idob = $request->fetchAll(PDO::FETCH_OBJ);
    
    //dump($idob);
    
    return($idob);
}
    

//Database : enregistrement / suppression d'un moment utilisateur

   
function StoreMoment($table,$id_ad,$type)
{        
    //insertion du moment utilisateur
    $db = initDb();
    
    $momid = preg_split('/-/', $table[0]);
    if ($type == 'MOMENT'){
        $mom = getMomentId($momid[0]);
        $title = $mom->title;
    }
    else $title = $type;

    
    $referent = MOMENT_USER;
    $iconLink = "./assets/images/img_users/smiley.png";
    
    $req = $db->prepare("
                    INSERT INTO moment(title,referent,iconLink)
                    VALUES (:title, :referent, :iconLink)
                ");
    
    $req->execute(array(
                        ':title' => $title,
                        ':referent' => $referent,
                        ':iconLink' => $iconLink,
                        ));

    $req = $db->prepare("SELECT id FROM moment WHERE id=LAST_INSERT_ID()");    
    $req->execute();
    $id_moment = $req->fetch(PDO::FETCH_OBJ);
   
    //mise a jour mobox
    
    $req = $db->prepare("
                    INSERT INTO mobox(id_moment,id_object,id_ad)
                    VALUES (:id_moment, :id_object, :id_ad)
                ");
    
    foreach ($table as $t)
    {
        $s = preg_split('/-/', $t);
        $req->execute(array(
                            ':id_moment' => $id_moment->id,
                            ':id_object' => $s[1],
                            ':id_ad' => $id_ad 
                            ));
    }
}

function DeleteMoment($mom)
{
     
    $m = preg_split('/ /', $mom);

    //efface le moment et l'index dans mobox
    
    $db = initDb();

    $request = $db->prepare("DELETE FROM moment WHERE id=?");
    $request->execute([$m[1]]);
    $request = $db->prepare("DELETE FROM mobox WHERE id_moment=?");
    $request->execute([$m[1]]);

}

//Database : gestion de la CHATROOM & MESSAGES

function GetChat($idUser)
{
    $db = initDb();
        
    $request = $db->prepare("SELECT id,idUserRight,idUserLeft,idAdRight,idAdLeft,dealRight,dealLeft FROM `chatroom` WHERE idUserRight = ? OR idUserLeft = ?");
    $request->execute([$idUser,$idUser]);
    return($request);
}

function NextChat($request)
{
    $chat = $request->fetch(PDO::FETCH_OBJ);
    return($chat);
}

function storeChat(int $idUserRight, int $idUserLeft, int $idAdRight, int $idAdLeft)
{
    $db = initDb();

    $request = $db->prepare("INSERT INTO chatroom(idUserRight, idUserLeft, idAdRight, idAdLeft) VALUES (:idUserRight,:idUserLeft,:idAdRight,:idAdLeft)");

    $request->execute(array(
                            ':idUserRight' => $idUserRight,
                            ':idUserLeft' => $idUserLeft,
                            ':idAdRight' => $idAdRight,
                            ':idAdLeft' => $idAdLeft
                            ));
}

function StoreTweet($idChat,$idUser,$text){
    
    $db = initDb();
    
    $req = $db->prepare("INSERT INTO message(idChatroom, idUser, text) VALUES (?, ?, ?)");
    $req->execute([$idChat,$idUser,$text]);
}

function GetTweet($idChat){
    
    $db = initDb();
    
    $req = $db->prepare("SELECT text,idUser,date FROM message WHERE idChatroom = ? ORDER BY date DESC");    
    $req->execute([$idChat]);
    
    $twe = $req->fetchAll(PDO::FETCH_OBJ);
    return($twe);
}

function StoreDeal($idChat,$deal,$side){
    
    $db = initDb();
    
    
    if ($side == CHAT_RIGHT) { $tmp = "UPDATE chatroom SET dealRight = ? WHERE id = ?"; /*echo "STORE DEAL RIGHT";*/ }
    else { $tmp = "UPDATE chatroom SET dealLeft = ? WHERE id = ?"; /*echo "STORE DEAL LEFT";*/ }
    
    $req = $db->prepare($tmp);
    $req->execute([$deal,$idChat]);
}

function incDeal($deal){
    
    if ($deal == DEAL_DONE) $my_deal = DEAL_NONE;
    else $deal++;

    return($deal);
}


