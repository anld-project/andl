<?php
$title = "Page administrateur";
include('Partials/head.php'); ?>

<body style="background-color: rgb(51, 54, 62);">

<?php include('Partials/menuBarConnected.php'); ?>

<div class ="mainAdministrator h-100 container d-flex justify-content-center">
    <div class="mainAdministratorChild">
        <section class="titleAdminPage justify-content-center">
            <div>
                <article>Bienvenue sur la page administrateur</article>
            </div>
        </section>
        <section id="centralBlockAdmin">
            <div id="chang_commands">
                <div class="chang_com">
                    <div>
                        <form>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Pourcentage de la commission :</label>
                                <input type="decimal" class="form-control" id="commission_changed" placeholder="8,1">
                            </div>
                        </form>
                    </div>
                </div>
                <div id= "validity_ad">
                    <form>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Durée de validité des annonces (en jours) :</label>
                            <input type="number" class="form-control" id="period_validity_ad" placeholder="210">
                        </div>
                    </form>
                </div>
            </div>
            <div id="imgRocketAdmin">
                <div id="imgRocketAdminStar">
                    <div>
                        <img src="assets/images/img_admin/star-1957919.svg" width="22px">
                    </div>
                </div>
                <div id="imgRocketAdminRocket">
                    <div>
                        <img src="assets/images/img_admin/rocket_admin.png">
                    </div>
                </div>
            </div>
        </section>
        <section id="btn_admin_valid">
            <div>
                <button type="button" class="btn btn-light">Mettre à jour</button>
            </div>
        </section>
    </div>
</div>

<?php include('Partials/scriptLinksBootstrap.php'); ?>
