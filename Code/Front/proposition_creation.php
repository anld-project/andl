<?php
$title = "Editer une proposition";
include('Partials/head.php'); ?>

<body>

<?php include('Partials/menuBarConnected.php'); ?>

<div class="mainBlockCreatProposition">
    <section class="titlePropCreation">
        <h4>Votre aide est précieuse</h4>
    </section>
    <section class="mainBlockPropCreat">
        <div class="mainChildBlockPropCreat">
            <div id="choicePropCreat">
                <div id="titleChoicePropCreat">
                    <article>Comment souhaitez vous aider la communauté?</article>
                </div>
                <div id="btnRadioChoicePropCreat">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineSevice5"
                               value="option1">
                        <label class="form-check-label" for="inlineRadio1">Service</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineLocation5"
                               value="option2">
                        <label class="form-check-label" for="inlineRadio2">Location</label>
                    </div>
                </div>
            </div>
            <div id="categoryPropCreat">
                <div id="titleChoicePropCreat">
                    <article>Votre catégorie</article>
                </div>
                <div id="ChoicePropCreatCateg">
                    <form>
                        <div class="form-group">
                            <!-- <label for="exampleFormControlInput1"></label> -->
                            <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Cours de guitare">
                        </div>
                    </form>
                </div>
            </div>
            <div id="descripPropCreat">
                <div id="titleChoicePropCreat">
                    <article>Décrivez votre offre</article>
                </div>
                <div class="top-modify-descrip">
                    <div class="descripCreatProposition6">
                        <form>
                            <div class="form-group">
                                <textarea class="form-control" id="descripCreatProp76" rows="3" placeholder="Bonjour,"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="photoPropCreat">
            
                        <div id="titleChoicePropCreat">
                            <article>Vos photos</article>
                        </div>

             
                            <label class="drop_target" for="inputFile">
                                <div class="image_preview">
                                    <div>
                                        <i class="fas fa-camera-retro fa-2x"></i>
                                    </div>
                                </div>
                                <input id="inputFile" type="file"/>
                            </label>
                    
                    
                    <div id="conditionsPropCreat">
                        <div id="titleChoicePropCreat">
                            <article>Vos conditions</article>
                        </div>
                        <section class="childConditionsPropCreat">
                            <div id="childConditionsPropCreat">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-secondary active">
                                        <input type="radio" name="options" id="option1" checked> Heure
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="options" id="option2"> Jour
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="options" id="option3"> Global
                                    </label>
                                </div>
                            </div>
                            <div id="choicePriceConditions">
                                <select class="custom-select mr-sm-2" id="conditionsPriceCreat">
                                    <option selected>5 €</option>
                                    <option value="1">10 €</option>
                                    <option value="2">15 €</option>
                                    <option value="3">20 €</option>
                                    <option value="4">25 €</option>
                                    <option value="5">30 €</option>
                                    <option value="6">35 €</option>
                                    <option value="7">40 €</option>
                                    <option value="8">45 €</option>
                                    <option value="9">50 €</option>
                                    <option value="10">60 €</option>
                                    <option value="11">70 €</option>
                                    <option value="12">80 €</option>
                                    <option value="13">90 €</option>
                                    <option value="14">100 €</option>
                                    <option value="15">120 €</option>
                                    <option value="16">150 €</option>
                                    <option value="17">200 €</option>
                                </select>
                            </div>
                        </section>
                    </div>
                    <section class="mainPostCreatProp">
                        <div id="btnPostCreatProp">
                            <button type="button" class="btn btn-primary">Poster</button>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</div>

<?php 
    include('Partials/footer.php');
    include('Partials/scriptLinksBootstrap.php');
?>
