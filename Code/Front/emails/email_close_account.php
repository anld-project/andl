<?php
$title = "Fermeture de compte";
include('../Partials/head.php'); ?>


<body style="background: linear-gradient(135deg,#fae5f9 , #ddfeda);">

<div class="mainEmailCloseAccount">
    <section class="logoTopEmailPay">
        <img src="../assets/images/logos/logo-vopus-accroche.gif">
    </section>
    <section class="firstNameEmailCloseAcc">
        <article>Bonjour <span>Prénom</span> !</article>
    </section>
    <section class="blockCentralEmailCloseAcc">
        <div>
            <article>Nous regrettons votre décision de nous quitter
                et nous espérons vous revoir bientôt.
            </article>
        </div>
        <div>
            <article>Conformément à votre souhait, votre compte sera définitivement
                clôturé dès la réception de ce mail.
            </article>
        </div>
    </section>

    <section class="byeEmailPay">
        <div class="email_infos_deal_child">
            <article>Salutations</article>
        </div>
        <div class="email_infos_deal_child">
            <article>L'équipe <em>VOPUS</em></article>
        </div>
    </section>
    <section class="emailPayFooter">
        <div id="logo_bottom_email_Pay">
            <img src="../assets/images/logos/logo_vopus_simple.gif">
        </div>
        <div class="email_pay_socialMedia">
            <div>
                <img src="../assets/images/social_media/instagram.png">
            </div>
            <div>
                <img src="../assets/images/social_media/facebook.png">
            </div>
            <div>
                <img src="../assets/images/social_media/youtube.png">
            </div>
        </div>
    </section>
</div>

<?php include('../Partials/scriptLinksBootstrap.php'); ?>
