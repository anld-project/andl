<?php
$title = "Oubli de mot de passe";
include('../Partials/head.php'); ?>

<body style="background: linear-gradient(135deg,#fae5f9 , #ddfeda);">

<div class="mainEmailPassForg">
    <section class="logoTopEmailPay">
        <img src="../assets/images/logos/logo-vopus-accroche.gif">
    </section>
    <section class="firstNameEmailForgPass">
        <article>Bonjour <span>Prénom</span> !</article>
    </section>
    <section class="blockCentralEmailForgPass">
        <div>
            <article>Vous avez oublié votre mot de passe? Pas de panique</article>
        </div>
        <div class="alert alert-success" role="alert">
            <a href="#" class="alert-link">Afin de le renouveler cliquez ici</a>
        </div>
    </section>
    <section class="byeEmailPay">
        <div class="email_infos_deal_child">
            <article>A bientôt</article>
        </div>
        <div class="email_infos_deal_child">
            <article>L'équipe <em>VOPUS</em></article>
        </div>
    </section>
    <section class="emailPayFooter">
        <div id="logo_bottom_email_Pay">
            <img src="../assets/images/logos/logo_vopus_simple.gif">
        </div>
        <div class="email_pay_socialMedia">
            <div>
                <img src="../assets/images/social_media/instagram.png">
            </div>
            <div>
                <img src="../assets/images/social_media/facebook.png">
            </div>
            <div>
                <img src="../assets/images/social_media/youtube.png">
            </div>
        </div>
    </section>
</div>

<?php include('../Partials/scriptLinksBootstrap.php'); ?>
