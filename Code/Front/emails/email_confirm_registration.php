<?php
$title = "Confirmation d'inscription";
include('../Partials/head.php'); ?>

<body style="background: linear-gradient(135deg,#fae5f9 , #ddfeda);">

<div class="mainEmailConfRegis">
    <section class="logoTopEmailPay">
        <img src="../assets/images/logos/logo-vopus-accroche.gif">
    </section>
    <section class="firstNameEmailPay">
        <article>Bienvenue <span>Prénom</span> !</article>
    </section>
    <section class="welcome_email">
        <div>
            <article>Tu fais désormais partie de la grande communauté de <em>VOPUS</em> !</article>
        </div>
        <div>
            <article>La planète te remercie !</article>
        </div>
    </section>
    <section id="email_infos_regis">

        <div id="subTitle_email_Conf_Regis">
            <article>Ton adresse mail: <strong>elodie.michu123@free.fr</strong></article>
        </div>
        <div class="subTitle_email_Conf_Regis">
            <article>Tu peux à tout moment modifier ton mot de passe</article>
        </div>
        <div class="alert alert-success" role="alert">
            <a href="#" class="alert-link">Modifier mon mot de passe</a>
        </div>

    </section>

    <section class="byeEmailPay">
        <div class="email_infos_deal_child">
            <article>A bientôt</article>
        </div>
        <div class="email_infos_deal_child">
            <article>L'équipe <em>VOPUS</em></article>
        </div>
    </section>
    <section class="emailPayFooter">
        <div id="logo_bottom_email_Pay">
            <img src="../assets/images/logos/logo_vopus_simple.gif">
        </div>
        <div class="email_pay_socialMedia">
            <div>
                <img src="../assets/images/social_media/instagram.png">
            </div>
            <div>
                <img src="../assets/images/social_media/facebook.png">
            </div>
            <div>
                <img src="../assets/images/social_media/youtube.png">
            </div>
        </div>
    </section>
</div>


<?php include('../Partials/scriptLinksBootstrap.php'); ?>
