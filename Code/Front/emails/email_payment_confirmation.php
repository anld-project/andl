<?php
$title = "Confirmation de paiement";
include('../Partials/head.php'); ?>

<body style="background: linear-gradient(135deg,#fae5f9 , #ddfeda);">

<div class="mainEmailPay">
    <section class="logoTopEmailPay">
        <img src="../assets/images/logos/logo-vopus-accroche.gif">
    </section>
    <section class="firstNameEmailPay">
        <article>Bonjour <span>Prénom</span> !</article>
    </section>
    <section class="thank-dev">
        <article>Merci ! Tu agis pour le développement durable !</article>
    </section>
    <section id="email_infos_deal">

        <div id="subTitle_email_pay">
            <article>Ton DEAL avec <span>Alain</span> pour <span>titre de l'annonce</span></article>
        </div>
        <div class="email_infos_deal_child">
            <article>Prix: <span>20€ / jour</span></article>
        </div>
        <div class="email_infos_deal_child">
            <article>Paiement: <span>PayPal</span></article>
        </div>
        <div class="email_infos_deal_child">
            <article>Temps estimé: <span>2 jours</span></article>
        </div>

        <!--Si le DEAL est une location : -->

        <div class="email_infos_deal_child">
            <article>Caution: <span>30€</span></article>
        </div>

    </section>
    <section id="btnCheckMyDeal">
        <div>
            <a href="../my_deals.php">
                <button type="button" class="btn btn-outline-dark">Voir mon DEAL</button>
            </a>
        </div>
    </section>
    <section class="byeEmailPay">
        <div class="email_infos_deal_child">
            <article>A bientôt</article>
        </div>
        <div class="email_infos_deal_child">
            <article>L'équipe <em>VOPUS</em></article>
        </div>
    </section>
    <section class="emailPayFooter">
        <div id="logo_bottom_email_Pay">
            <img src="../assets/images/logos/logo_vopus_simple.gif">
        </div>
        <div class="email_pay_socialMedia">
            <div>
                <img src="../assets/images/social_media/instagram.png">
            </div>
            <div>
                <img src="../assets/images/social_media/facebook.png">
            </div>
            <div>
                <img src="../assets/images/social_media/youtube.png">
            </div>
        </div>
    </section>
</div>

<?php include('../Partials/scriptLinksBootstrap.php'); ?>