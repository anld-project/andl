<?php

require_once('functions.php');
require_once('./assets/class/user.class.php');
include('partials/head.php');

    //extraction des infos annonce slider page acceuil
        
    $idAdLastProp = $_SESSION['idAdLastProp'];
    $ad = GetAdId($idAdLastProp);
    $adobj = GetIdObject(GetAdMoment($ad)->id_object);

    //extraction des infos user courant

    $idUser = $_SESSION['idUser'];
    $user = new user($idUser);
    $obj = $user->NextUser();

    //creation nouvelle annonce

    $new_ad_id = storeAd(   0,
                            $idUser,
                            ($ad->needBidAttribute == 'NEED'?'BID':'NEED'),
                            $ad->leasingSaleAttribute,
                            $ad->momentObjectAttribute,
                            AVAILABLE,
                            $_SESSION['deal_text'],
                            budgetPriceConvert($_SESSION['option_budget']),
                            $_SESSION['priceAttribute'],
                            $_SESSION['option_duration']
                        );

    //creation de la chatroom


    storeChat(
            $idUser,
            $ad->idUser,
            $new_ad_id,
            $ad->id
           );

    //creation du moment objet

    $table[0] = '0-'.$adobj->id;   //(0 pas de moment de vie pluriel)
    StoreMoment($table,$new_ad_id,'OBJECT');   //on stocke un moment de vie singulier (objet)

    redirect("chatroom.php");
?>
